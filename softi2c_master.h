/** library to communicate using I²C as master, implemented in software
 *  @file
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @copyright SPDX-License-Identifier: GPL-3.0-or-later
 *  @date 2021
 */
#pragma once

/** setup I²C peripheral
 *  @param[in] freq_khz desired clock frequency, in kHz
 *  @return if I²C bus is ready
 */
bool softi2c_master_setup(uint16_t freq_khz);
/** release I²C peripheral */
void softi2c_master_release(void);
/** send start condition
 *  @return if start sent (else arbitration lost)
 */
bool softi2c_master_start(void);
/** sent stop condition
 *  @param[in] i2c I²C base address
 *  @return if stop sent (else arbitration lost)
 */
bool softi2c_master_stop(void);
/** select I²C slave device
 *  @param[in] slave I²C address of slave device to select
 *  @param[in] write this transaction will be followed by a read (false) or write (true) operation
 *  @return if slave ACKed
 *  @note includes (re-)start condition
 */
bool softi2c_master_select_slave(uint8_t slave, bool write);
/** read data over I²C
 *  @param[out] data array to store bytes read
 *  @param[in] data_size number of bytes to read
 *  @return if read succeeded (else arbitration lost)
 *  @warning the slave device must be selected before this operation
 *  @note includes sending stop (after having NACKed last received byte)
 */
bool softi2c_master_read(uint8_t* data, uint16_t data_size);
/** write data over I²C
 *  @param[in] data array of byte to write to slave
 *  @param[in] data_size number of bytes to write
 *  @return if write succeeded (else data has been NACKed)
 *  @warning the slave device must be selected before this operation
 *  @note no stop condition is sent at the end, allowing multiple writes
 */
bool softi2c_master_write(const uint8_t* data, uint16_t data_size);
/** read data from slave device
 *  @param[in] slave I²C address of slave device to select
 *  @param[out] data array to store bytes read
 *  @param[in] data_size number of bytes to read
 *  @return if read succeeded (else arbitration has been lost)
 *  @note start and stop conditions are included
 */
bool softi2c_master_slave_read(uint8_t slave, uint8_t* data, uint16_t data_size);
/** write data to slave device
 *  @param[in] slave I²C address of slave device to select
 *  @param[in] data array of byte to write to slave
 *  @param[in] data_size number of bytes to write
 *  @return if write succeeded
 *  @note start and stop conditions are included
 */
bool softi2c_master_slave_write(uint8_t slave, const uint8_t* data, uint16_t data_size);
/** read data at specific address from an I²C memory slave
 *  @param[in] slave I²C address of slave device to select
 *  @param[in] address memory address of slave to read from
 *  @param[in] address_size address size in bytes
 *  @param[out] data array to store bytes read
 *  @param[in] data_size number of bytes to read
 *  @return if read succeeded
 *  @note start and stop conditions are included
 */
bool softi2c_master_address_read(uint8_t slave, const uint8_t* address, uint16_t address_size, uint8_t* data, uint16_t data_size);
/** write data at specific address on an I²C memory slave
 *  @param[in] slave I²C address of slave device to select
 *  @param[in] address memory address of slave to write to
 *  @param[in] address_size address size in bytes
 *  @param[in] data array of byte to write to slave
 *  @param[in] data_size number of bytes to write
 *  @return if write succeeded
 *  @note start and stop conditions are included
 */
bool softi2c_master_address_write(uint8_t slave, const uint8_t* address, uint16_t address_size, const uint8_t* data, uint16_t data_size);
