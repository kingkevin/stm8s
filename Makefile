CC := sdcc
CFLAGS := -mstm8 --std-c99 --opt-code-size --Werror
LDFLAGS = -mstm8 --out-fmt-ihx -lstm8
FIRMWARE := main
SRC_FILES := $(wildcard *.c)
OBJ_FILES := $(patsubst %.c,%.rel,$(SRC_FILES))

all: $(FIRMWARE).ihx

$(FIRMWARE).ihx: $(OBJ_FILES)
	$(CC) $(LDFLAGS) $^ -o $@
	size $@

%.rel: %.c %.h
	$(CC) $(CFLAGS) --compile-only $<

flash: $(FIRMWARE).ihx
	stm8flash -c stlinkv2 -p stm8s103f3 -w $<

clean:
	rm -f $(FIRMWARE).asm $(FIRMWARE).ihx $(FIRMWARE).cdb $(FIRMWARE).lst $(FIRMWARE).map $(FIRMWARE).lk $(FIRMWARE).rel $(FIRMWARE).rst $(FIRMWARE).sym $(OBJ_FILES)
