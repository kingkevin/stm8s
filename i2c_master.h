/** library to communicate using I²C as master
 *  @file
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @copyright SPDX-License-Identifier: GPL-3.0-or-later
 *  @date 2017-2021
 *  @warning the I²C peripheral is very glitchy (sending random clock pulses), thus prefer the software implementation alternative, which is simpler, more flexible, smaller, and very stable (it just draws more energy)
 */
#pragma once

/** I²C return codes */
enum i2c_master_rc {
	I2C_MASTER_RC_NONE = 0, /**< no error */
	I2C_MASTER_RC_START_STOP_IN_PROGESS, /**< a start or stop condition is already in progress */
	I2C_MASTER_RC_NOT_MASTER, /**< not in master mode */
	I2C_MASTER_RC_NOT_TRANSMIT, /**< not in transmit mode */
	I2C_MASTER_RC_NOT_RECEIVE, /**< not in receive mode */
	I2C_MASTER_RC_NOT_READY, /**< slave is not read (previous operations has been NACKed) */
	I2C_MASTER_RC_NAK, /**< not acknowledge received */
	I2C_MASTER_RC_BUS_ERROR, /**< an error on the I²C bus occurred */
	I2C_MASTER_RC_TIMEOUT, /**< a timeout has occurred because an operation has not completed in the expected time */
	I2C_MASTER_RC_OTHER, /** any other error (does not have to be I²C related) */
};

/** setup I²C peripheral
 *  @param[in] freq_khz desired clock frequency, in kHz
 *  @return if I²C bus is ready to be used (same as i2c_master_check_signals)
 */
bool i2c_master_setup(uint16_t freq_khz);
/** release I²C peripheral */
void i2c_master_release(void);
/** reset I²C peripheral, fixing any locked state
 *  @warning the I²C peripheral needs to be re-setup
 *  @note to be used after failed start or stop, and bus error
 */
void i2c_master_reset(void);
/** check if SDA and SCL signals are pulled high
 *  @return if SDA and SCL signals are pulled high
 */
bool i2c_master_check_signals(void);
/** send start condition
 *  @return I2C return code
 */
enum i2c_master_rc i2c_master_start(void);
/** select I²C slave device
 *  @warning a start condition should be sent before this operation
 *  @param[in] slave I²C address of slave device to select
 *  @param[in] address_10bit if the I²C slave address is 10 bits wide
 *  @param[in] write this transaction will be followed by a read (false) or write (true) operation
 *  @return I²C return code
 */
enum i2c_master_rc i2c_master_select_slave(uint16_t slave, bool address_10bit, bool write);
/** read data over I²C
 *  @param[out] data array to store bytes read
 *  @param[in] data_size number of bytes to read
 *  @return I²C return code
 *  @warning the slave device must be selected before this operation
 *  @note a stop condition will be sent at the end (I²C does not permit multiple reads, and this is necessary for 1-byte transfer)
 */
enum i2c_master_rc i2c_master_read(uint8_t* data, uint16_t data_size);
/** write data over I²C
 *  @param[in] data array of byte to write to slave
 *  @param[in] data_size number of bytes to write
 *  @return I²C return code
 *  @warning the slave device must be selected before this operation
 *  @note no stop condition is sent at the end, allowing multiple writes
 */
enum i2c_master_rc i2c_master_write(const uint8_t* data, uint16_t data_size);
/** sent stop condition
 *  @param[in] i2c I²C base address
 *  @return I²C return code
 */
enum i2c_master_rc i2c_master_stop(void);
/** read data from slave device
 *  @param[in] slave I²C address of slave device to select
 *  @param[in] address_10bit if the I²C slave address is 10 bits wide
 *  @param[out] data array to store bytes read
 *  @param[in] data_size number of bytes to read
 *  @return I²C return code
 *  @note start and stop conditions are included
 */
enum i2c_master_rc i2c_master_slave_read(uint16_t slave, bool address_10bit, uint8_t* data, uint16_t data_size);
/** write data to slave device
 *  @param[in] slave I²C address of slave device to select
 *  @param[in] address_10bit if the I²C slave address is 10 bits wide
 *  @param[in] data array of byte to write to slave
 *  @param[in] data_size number of bytes to write
 *  @return I²C return code
 *  @note start and stop conditions are included
 */
enum i2c_master_rc i2c_master_slave_write(uint16_t slave, bool address_10bit, const uint8_t* data, uint16_t data_size);
/** read data at specific address from an I²C memory slave
 *  @param[in] slave I²C address of slave device to select
 *  @param[in] address_10bit if the I²C slave address is 10 bits wide
 *  @param[in] address memory address of slave to read from
 *  @param[in] address_size address size in bytes
 *  @param[out] data array to store bytes read
 *  @param[in] data_size number of bytes to read
 *  @return I²C return code
 *  @note start and stop conditions are included
 */
enum i2c_master_rc i2c_master_address_read(uint16_t slave, bool address_10bit, const uint8_t* address, uint16_t address_size, uint8_t* data, uint16_t data_size);
/** write data at specific address on an I²C memory slave
 *  @param[in] slave I²C address of slave device to select
 *  @param[in] address_10bit if the I²C slave address is 10 bits wide
 *  @param[in] address memory address of slave to write to
 *  @param[in] address_size address size in bytes
 *  @param[in] data array of byte to write to slave
 *  @param[in] data_size number of bytes to write
 *  @return I²C return code
 *  @note start and stop conditions are included
 */
enum i2c_master_rc i2c_master_address_write(uint16_t slave, bool address_10bit, const uint8_t* address, uint16_t address_size, const uint8_t* data, uint16_t data_size);
/** interrupt service routine used to wake up
 *  @note not sure why the declaration need to be in main for it to work
 */
void i2c_master_isr(void) __interrupt(IRQ_I2C);

