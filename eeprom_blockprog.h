/** library to program EEPROM using block programming
 *  @file
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @copyright SPDX-License-Identifier: GPL-3.0-or-later
 *  @date 2021
 *  @warning functions need to be put in and run from RAM (because block programming is used)
 */

/** program EEPROM using block programming
 *  @param[in] data data to be programmed
 *  @param[in] length length of data to be programmed (must be a multiple of the block length)
 *  @return if program succeeded
 */
bool eeprom_blockprog(const uint8_t* data, uint16_t length);
