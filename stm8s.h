/* STM8S103 register definition
 * the register names and content are from STM8S reference manual (Revision 17, October 2017)
 * the register base addresses (search for _BASE) and some offsets (search PRODUCT) are from STM8S103 datasheet (February 2017) and specific to this device
 * the SWIM register is described in STM8 SWIM communication protocol and debug module (UM0470, August 2016)
 * using the macro definitions saves space, while using the structures is safer
 */
#include <stdint.h>

// I/O port hardware register map
typedef union {
	struct {
		uint8_t ODR0:1; /*!< bit 0: Output data register bit */
		uint8_t ODR1:1; /*!< bit 1: Output data register bit */
		uint8_t ODR2:1; /*!< bit 2: Output data register bit */
		uint8_t ODR3:1; /*!< bit 3: Output data register bit */
		uint8_t ODR4:1; /*!< bit 4: Output data register bit */
		uint8_t ODR5:1; /*!< bit 5: Output data register bit */
		uint8_t ODR6:1; /*!< bit 6: Output data register bit */
		uint8_t ODR7:1; /*!< bit 7: Output data register bit */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} GPIO_Px_ODR_type; /*!< Port x output data register (Px_ODR) */

typedef union {
	struct {
		uint8_t IDR0:1; /*!< bit 0: Pin input value */
		uint8_t IDR1:1; /*!< bit 1: Pin input value */
		uint8_t IDR2:1; /*!< bit 2: Pin input value */
		uint8_t IDR3:1; /*!< bit 3: Pin input value */
		uint8_t IDR4:1; /*!< bit 4: Pin input value */
		uint8_t IDR5:1; /*!< bit 5: Pin input value */
		uint8_t IDR6:1; /*!< bit 6: Pin input value */
		uint8_t IDR7:1; /*!< bit 7: Pin input value */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} GPIO_Px_IDR_type; /*!< Port x pin input data register (Px_IDR) */

typedef union {
	struct {
		uint8_t DDR0:1; /*!< bit 0: Data direction bit */
		uint8_t DDR1:1; /*!< bit 1: Data direction bit */
		uint8_t DDR2:1; /*!< bit 2: Data direction bit */
		uint8_t DDR3:1; /*!< bit 3: Data direction bit */
		uint8_t DDR4:1; /*!< bit 4: Data direction bit */
		uint8_t DDR5:1; /*!< bit 5: Data direction bit */
		uint8_t DDR6:1; /*!< bit 6: Data direction bit */
		uint8_t DDR7:1; /*!< bit 7: Data direction bit */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} GPIO_Px_DDR_type; /*!< Port x data direction register */

typedef union {
	struct {
		uint8_t C10:1; /*!< bit 0: Control bit */
		uint8_t C11:1; /*!< bit 1: Control bit */
		uint8_t C12:1; /*!< bit 2: Control bit */
		uint8_t C13:1; /*!< bit 3: Control bit */
		uint8_t C14:1; /*!< bit 4: Control bit */
		uint8_t C15:1; /*!< bit 5: Control bit */
		uint8_t C16:1; /*!< bit 6: Control bit */
		uint8_t C17:1; /*!< bit 7: Control bit */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} GPIO_Px_CR1_type; /*!< Port x control register 1 (Px_CR1) */

typedef union {
	struct {
		uint8_t C20:1; /*!< bit 0: Control bit */
		uint8_t C21:1; /*!< bit 1: Control bit */
		uint8_t C22:1; /*!< bit 2: Control bit */
		uint8_t C23:1; /*!< bit 3: Control bit */
		uint8_t C24:1; /*!< bit 4: Control bit */
		uint8_t C25:1; /*!< bit 5: Control bit */
		uint8_t C26:1; /*!< bit 6: Control bit */
		uint8_t C27:1; /*!< bit 7: Control bit */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} GPIO_Px_CR2_type; /*!< Port x control register 2 (Px_CR2) */

typedef struct {
	volatile GPIO_Px_ODR_type ODR;
	volatile GPIO_Px_IDR_type IDR;
	volatile GPIO_Px_DDR_type DDR;
	volatile GPIO_Px_CR1_type CR1;
	volatile GPIO_Px_CR2_type CR2;
} GPIO_type;

// I/O port hardware register map
// Block: Port A
#define PA_BASE 0x5000
#define GPIO_PA ((GPIO_type*)PA_BASE)
#define PA_ODR (*(volatile uint8_t *)(PA_BASE + 0x00))
#define PA_IDR (*(volatile uint8_t *)(PA_BASE + 0x01))
#define PA_DDR (*(volatile uint8_t *)(PA_BASE + 0x02))
#define PA_CR1 (*(volatile uint8_t *)(PA_BASE + 0x03))
#define PA_CR2 (*(volatile uint8_t *)(PA_BASE + 0x04))
#define PA0 (1U << 0)
#define PA1 (1U << 1)
#define PA2 (1U << 2)
#define PA3 (1U << 3)
#define PA4 (1U << 4)
#define PA5 (1U << 5)
#define PA6 (1U << 6)
#define PA7 ((uint8_t)(1U << 7))
// Block: Port B
#define PB_BASE 0x5005
#define GPIO_PB ((GPIO_type*)PB_BASE)
#define PB_ODR (*(volatile uint8_t *)(PB_BASE + 0x00))
#define PB_IDR (*(volatile uint8_t *)(PB_BASE + 0x01))
#define PB_DDR (*(volatile uint8_t *)(PB_BASE + 0x02))
#define PB_CR1 (*(volatile uint8_t *)(PB_BASE + 0x03))
#define PB_CR2 (*(volatile uint8_t *)(PB_BASE + 0x04))
#define PB0 (1U << 0)
#define PB1 (1U << 1)
#define PB2 (1U << 2)
#define PB3 (1U << 3)
#define PB4 (1U << 4)
#define PB5 (1U << 5)
#define PB6 (1U << 6)
#define PB7 ((uint8_t)(1U << 7))
// Block: Port C
#define PC_BASE 0x500A
#define GPIO_PC ((GPIO_type*)PC_BASE)
#define PC_ODR (*(volatile uint8_t *)(PC_BASE + 0x00))
#define PC_IDR (*(volatile uint8_t *)(PC_BASE + 0x01))
#define PC_DDR (*(volatile uint8_t *)(PC_BASE + 0x02))
#define PC_CR1 (*(volatile uint8_t *)(PC_BASE + 0x03))
#define PC_CR2 (*(volatile uint8_t *)(PC_BASE + 0x04))
#define PC0 (1U << 0)
#define PC1 (1U << 1)
#define PC2 (1U << 2)
#define PC3 (1U << 3)
#define PC4 (1U << 4)
#define PC5 (1U << 5)
#define PC6 (1U << 6)
#define PC7 ((uint8_t)(1U << 7))
// Block: Port D
#define PD_BASE 0x500F
#define GPIO_PD ((GPIO_type*)PD_BASE)
#define PD_ODR (*(volatile uint8_t *)(PD_BASE + 0x00))
#define PD_IDR (*(volatile uint8_t *)(PD_BASE + 0x01))
#define PD_DDR (*(volatile uint8_t *)(PD_BASE + 0x02))
#define PD_CR1 (*(volatile uint8_t *)(PD_BASE + 0x03))
#define PD_CR2 (*(volatile uint8_t *)(PD_BASE + 0x04))
#define PD0 (1U << 0)
#define PD1 (1U << 1)
#define PD2 (1U << 2)
#define PD3 (1U << 3)
#define PD4 (1U << 4)
#define PD5 (1U << 5)
#define PD6 (1U << 6)
#define PD7 ((uint8_t)(1U << 7))
// Block: Port E
#define PE_BASE 0x5014
#define GPIO_PE ((GPIO_type*)PE_BASE)
#define PE_ODR (*(volatile uint8_t *)(PE_BASE + 0x00))
#define PE_IDR (*(volatile uint8_t *)(PE_BASE + 0x01))
#define PE_DDR (*(volatile uint8_t *)(PE_BASE + 0x02))
#define PE_CR1 (*(volatile uint8_t *)(PE_BASE + 0x03))
#define PE_CR2 (*(volatile uint8_t *)(PE_BASE + 0x04))
#define PE0 (1U << 0)
#define PE1 (1U << 1)
#define PE2 (1U << 2)
#define PE3 (1U << 3)
#define PE4 (1U << 4)
#define PE5 (1U << 5)
#define PE6 (1U << 6)
#define PE7 ((uint8_t)(1U << 7))
// Block: Port F
#define PF_BASE 0x5019
#define GPIO_PF ((GPIO_type*)PF_BASE)
#define PF_ODR (*(volatile uint8_t *)(PF_BASE + 0x00))
#define PF_IDR (*(volatile uint8_t *)(PF_BASE + 0x01))
#define PF_DDR (*(volatile uint8_t *)(PF_BASE + 0x02))
#define PF_CR1 (*(volatile uint8_t *)(PF_BASE + 0x03))
#define PF_CR2 (*(volatile uint8_t *)(PF_BASE + 0x04))
#define PF0 (1U << 0)
#define PF1 (1U << 1)
#define PF2 (1U << 2)
#define PF3 (1U << 3)
#define PF4 (1U << 4)
#define PF5 (1U << 5)
#define PF6 (1U << 6)
#define PF7 ((uint8_t)(1U << 7))

// General hardware register map
// Block: Flash
typedef union {
	struct {
		uint8_t FIX:1; /*!< bit 0: Fixed Byte programming time */
		uint8_t IE:1; /*!< bit 1: Flash Interrupt enable */
		uint8_t AHALT:1; /*!< bit 2: Power-down in Active-halt mode */
		uint8_t HALT:1; /*!< bit 3: Power-down in Halt mode */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} FLASH_CR1_type; /*!< Flash control register 1 (FLASH_CR1) */

typedef union {
	struct {
		uint8_t PRG:1; /*!< bit 0: Standard block programming */
		const uint8_t :3; /*!< bit 3..1: Reserved */
		uint8_t FPRG:1; /*!< bit 4: Fast block programming */
		uint8_t ERASE:1; /*!< bit 5: Block erasing */
		uint8_t WPRG:1; /*!< bit 6: Word programming */
		uint8_t OPT:1; /*!< bit 7: Write option bytes */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} FLASH_CR2_type; /*!< Flash control register 2 (FLASH_CR2) */

typedef union {
	struct {
		uint8_t NPRG:1; /*!< bit 0: Standard block programming */
		uint8_t :3; /*!< bit 1..3: Reserved */
		uint8_t NFPRG:1; /*!< bit 4: Fast block programming */
		uint8_t NERASE:1; /*!< bit 5: Block erasing */
		uint8_t NWPRG:1; /*!< bit 6: Word programming */
		uint8_t NOPT:1; /*!< bit 7: Write option bytes */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} FLASH_NCR2_type; /*!< Flash complementary control register 2 (FLASH_NCR2) */

typedef union {
	struct {
		const uint8_t WPB0:1; /*!< bit 0: User boot code area protection bit */
		const uint8_t WPB1:1; /*!< bit 1: User boot code area protection bit */
		const uint8_t WPB2:1; /*!< bit 2: User boot code area protection bit */
		const uint8_t WPB3:1; /*!< bit 3: User boot code area protection bit */
		const uint8_t WPB4:1; /*!< bit 4: User boot code area protection bit */
		const uint8_t WPB5:1; /*!< bit 5: User boot code area protection bit */
	} fields; /*!< structure used for bit access */
	const uint8_t reg; /*!< type used for register access */
} FLASH_FPR_type; /*!< Flash protection register (FLASH_FPR) */

typedef union {
	struct {
		const uint8_t NWPB0:1; /*!< bit 0: User boot code area protection bit */
		const uint8_t NWPB1:1; /*!< bit 1: User boot code area protection bit */
		const uint8_t NWPB2:1; /*!< bit 2: User boot code area protection bit */
		const uint8_t NWPB3:1; /*!< bit 3: User boot code area protection bit */
		const uint8_t NWPB4:1; /*!< bit 4: User boot code area protection bit */
		const uint8_t NWPB5:1; /*!< bit 5: User boot code area protection bit */
	} fields; /*!< structure used for bit access */
	const uint8_t reg; /*!< type used for register access */
} FLASH_NFPR_type; /*!< Flash protection register (FLASH_NFPR) */

typedef union {
	struct {
		uint8_t PUK:8; /*!< bit 0..7: Main program memory unlock keys */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} FLASH_PUKR_type; /*!< Flash program memory unprotecting key register (FLASH_PUKR) */

typedef union {
	struct {
		uint8_t DUK:8; /*!< bit 0..7: Data EEPROM write unlock keys */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} FLASH_DUKR_type; /*!< Data EEPROM unprotection key register (FLASH_DUKR) */

typedef union {
	struct {
		uint8_t WR_PG_DIS:1; /*!< bit 0: Write attempted to protected page flag */
		uint8_t PUL:1; /*!< bit 1: Flash Program memory unlocked flag */
		uint8_t EOP:1; /*!< bit 2: End of programming (write or erase operation) flag */
		uint8_t DUL:1; /*!< bit 3: Data EEPROM area unlocked flag */
		const uint8_t :2; /*!< bit 4..5: Reserved, forced by hardware to 0 */
		const uint8_t HVOFF:1; /*!< bit 6: End of high voltage flag */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} FLASH_IAPSR_type; /*!< Flash status register (FLASH_IAPSR) */

typedef struct {
	volatile FLASH_CR1_type CR1;
	volatile FLASH_CR2_type CR2;
	volatile FLASH_NCR2_type NCR2;
	volatile FLASH_FPR_type FPR;
	volatile FLASH_NFPR_type NFPR;
	volatile FLASH_IAPSR_type IAPSR;
	const uint8_t reserved0[2];
	volatile FLASH_PUKR_type PUKR;
	const uint8_t reserved1[1];
	volatile FLASH_DUKR_type DUKR;
} FLASH_type;

#define FLASH_BASE 0x505A
#define FLASH ((FLASH_type*)FLASH_BASE)
#define FLASH_CR1 (*(volatile uint8_t *)(FLASH_BASE + 0x00))
#define FLASH_CR1_FIX (1U << 0)
#define FLASH_CR1_IE (1U << 1)
#define FLASH_CR1_AHALT (1U << 2)
#define FLASH_CR1_HALT (1U << 3)
#define FLASH_CR2 (*(volatile uint8_t *)(FLASH_BASE + 0x01))
#define FLASH_CR2_PRG (1U << 0)
#define FLASH_CR2_FPRG (1U << 4)
#define FLASH_CR2_ERASE (1U << 5)
#define FLASH_CR2_WPRG (1U << 6)
#define FLASH_CR2_OPT (1U << 7)
#define FLASH_NCR2 (*(volatile uint8_t *)(FLASH_BASE + 0x02))
#define FLASH_NCR2_NPRG (1U << 0)
#define FLASH_NCR2_NFPRG (1U << 4)
#define FLASH_NCR2_NERASE (1U << 5)
#define FLASH_NCR2_NWPRG (1U << 6)
#define FLASH_NCR2_NOPT (1U << 7)
#define FLASH_FPR (*(volatile uint8_t *)(FLASH_BASE + 0x03))
#define FLASH_FPR_WPB0 (1U << 0)
#define FLASH_FPR_WPB1 (1U << 1)
#define FLASH_FPR_WPB2 (1U << 2)
#define FLASH_FPR_WPB3 (1U << 3)
#define FLASH_FPR_WPB4 (1U << 4)
#define FLASH_FPR_WPB5 (1U << 5)
#define FLASH_NFPR (*(volatile uint8_t *)(FLASH_BASE + 0x04))
#define FLASH_NFPR_NWPB0 (1U << 0)
#define FLASH_NFPR_NWPB1 (1U << 1)
#define FLASH_NFPR_NWPB2 (1U << 2)
#define FLASH_NFPR_NWPB3 (1U << 3)
#define FLASH_NFPR_NWPB4 (1U << 4)
#define FLASH_NFPR_NWPB5 (1U << 5)
#define FLASH_IAPSR (*(volatile uint8_t *)(FLASH_BASE + 0x05))
#define FLASH_IAPSR_WR_PG_DIS (1U << 0)
#define FLASH_IAPSR_PUL (1U << 1)
#define FLASH_IAPSR_EOP (1U << 2)
#define FLASH_IAPSR_DUL (1U << 3)
#define FLASH_IAPSR_HVOFF (1U << 6)
#define FLASH_PUKR (*(volatile uint8_t *)(FLASH_BASE + 0x08))
#define FLASH_PUKR_KEY1 0x56
#define FLASH_PUKR_KEY2 0xAE
#define FLASH_DUKR (*(volatile uint8_t *)(FLASH_BASE + 0x0A))
#define FLASH_DUKR_KEY1 0xAE
#define FLASH_DUKR_KEY2 0x56

// Block: ITC

typedef union {
	struct {
		uint8_t PAIS:2; /*!< bit 0..1: Port A external interrupt sensitivity bits */
		uint8_t PBIS:2; /*!< bit 2..3: Port B external interrupt sensitivity bits */
		uint8_t PCIS:2; /*!< bit 4..5: Port C external interrupt sensitivity bits */
		uint8_t PDIS:2; /*!< bit 6..7: Port D external interrupt sensitivity bits */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} EXTI_CR1_type; /*!< External interrupt control register 1 (EXTI_CR1) */

typedef union {
	struct {
		uint8_t PEIS:2; /*!< bit 0..1: Port E external interrupt sensitivity bits */
		uint8_t TLIS:1; /*!< bit 2: Top level interrupt sensitivity */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} EXTI_CR2_type; /*!< External interrupt control register 2 (EXTI_CR2) */

typedef struct {
	volatile EXTI_CR1_type CR1;
	volatile EXTI_CR2_type CR2;
} EXTI_type;

#define EXTI_BASE 0x50A0
#define EXTI ((EXTI_type*)EXTI_BASE)
#define EXTI_CR1 (*(volatile uint8_t *)(EXTI_BASE + 0x00))
#define EXTI_CR1_PAIS_OFFSET 0
#define EXTI_CR1_PAIS_MASK 0x3
#define EXTI_CR1_PBIS_OFFSET 2
#define EXTI_CR1_PBIS_MASK 0x3
#define EXTI_CR1_PCIS_OFFSET 4
#define EXTI_CR1_PCIS_MASK 0x3
#define EXTI_CR1_PDIS_OFFSET 6
#define EXTI_CR1_PDIS_MASK 0x3
#define EXTI_CR2 (*(volatile uint8_t *)(EXTI_BASE + 0x01))
#define EXTI_CR2_PEIS_OFFSET 0
#define EXTI_CR2_PEIS_MASK 0x3
#define EXTI_CR2_TLIS (1U << 2)
#define EXTI_FALLING_EDGE_LOW_LEVEL 0
#define EXTI_RISING_EDGE 1
#define EXTI_FALLING_EDGE 2
#define EXTI_RISING_FALLING_EDGE 3

// Block: RST
typedef union {
	struct {
		uint8_t WWDGF:1; /*!< bit 0: Window Watchdog reset flag */
		uint8_t IWDGF:1; /*!< bit 1: Independent Watchdog reset flag */
		uint8_t ILLOPF:1; /*!< bit 2: Illegal opcode reset flag */
		uint8_t SWIMF:1; /*!< bit 3: SWIM reset flag */
		uint8_t EMCF:1; /*!< bit 4: EMC reset flag */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} RST_SR_type; /*!< Reset status register (RST_SR) */

typedef struct {
	volatile RST_SR_type SR;
} RST_type;

#define RST_BASE 0x50B3
#define RST ((RST_type*)RST_BASE)
#define RST_SR (*(volatile uint8_t *)(RST_BASE + 0x00))
#define RST_SR_WWDGF (1U << 0)
#define RST_SR_IWDGF (1U << 1)
#define RST_SR_ILLOPF (1U << 2)
#define RST_SR_SWIMF (1U << 3)
#define RST_SR_EMCF (1U << 4)

// Block: CLK
typedef union {
	struct {
		uint8_t HSIEN:1; /*!< bit 0: High speed internal RC oscillator enable */
		const uint8_t HSIRDY:1; /*!< bit 1: High speed internal oscillator ready */
		uint8_t FHWU:1; /*!< bit 2: Fast wakeup from Halt/Active-halt modes */
		uint8_t LSIEN:1; /*!< bit 3: Low speed internal RC oscillator enable */
		const uint8_t LSIRDY:1; /*!< bit 4: Low speed internal oscillator ready */
		uint8_t REGAH:1; /*!< bit 5: Regulator power off in Active-halt mode */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} CLK_ICKR_type; /*!< Internal clock register (CLK_ICKR) */

typedef union {
	struct {
		uint8_t HSEEN:1; /*!< bit 0: High speed external crystal oscillator enable */
		const uint8_t HSERDY:1; /*!< bit 1: High speed external crystal oscillator ready */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} CLK_ECKR_type; /*!< External clock register (CLK_ECKR) */

typedef union {
	struct {
		const uint8_t CKM:8; /*!< bit 0..7: Clock master status register (CLK_CMSR) */
	} fields; /*!< structure used for bit access */
	const uint8_t reg; /*!< type used for register access */
} CLK_CMSR_type; /*!< Clock master status register (CLK_CMSR) */

typedef union {
	struct {
		uint8_t SWI:8; /*!< bit 0..7: Clock master selection bits */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} CLK_SWR_type; /*!< Clock master switch register (CLK_SWR) */

typedef union {
	struct {
		uint8_t SWBSY:1; /*!< bit 0: Switch busy */
		uint8_t SWEN:1; /*!< bit 1: Switch start/stop */
		uint8_t SWIEN:1; /*!< bit 2: Clock switch interrupt enable */
		uint8_t SWIF:1; /*!< bit 3: Clock switch interrupt flag */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} CLK_SWCR_type; /*!< Switch control register (CLK_SWCR) */

typedef union {
	struct {
		uint8_t CPUDIV:3; /*!< bit 0..2: CPU clock prescaler */
		uint8_t HSIDIV:2; /*!< bit 3..4: High speed internal clock prescaler */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} CLK_CKDIVR_type; /*!<  */

typedef union {
	struct {
		uint8_t PCKEN10:1; /*!< bit 0: Peripheral clock gating bit */
		uint8_t PCKEN11:1; /*!< bit 1: Peripheral clock gating bit */
		uint8_t PCKEN12:1; /*!< bit 2: Peripheral clock gating bit */
		uint8_t PCKEN13:1; /*!< bit 3: Peripheral clock gating bit */
		uint8_t PCKEN14:1; /*!< bit 4: Peripheral clock gating bit */
		uint8_t PCKEN15:1; /*!< bit 5: Peripheral clock gating bit */
		uint8_t PCKEN16:1; /*!< bit 6: Peripheral clock gating bit */
		uint8_t PCKEN17:1; /*!< bit 7: Peripheral clock gating bit */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} CLK_PCKENR1_type; /*!< Peripheral clock gating register 2 (CLK_PCKENR1) */

typedef union {
	struct {
		uint8_t PCKEN20:1; /*!< bit 0: Peripheral clock gating bit */
		uint8_t PCKEN21:1; /*!< bit 1: Peripheral clock gating bit */
		uint8_t PCKEN22:1; /*!< bit 2: Peripheral clock gating bit */
		uint8_t PCKEN23:1; /*!< bit 3: Peripheral clock gating bit */
		uint8_t PCKEN24:1; /*!< bit 4: Peripheral clock gating bit */
		uint8_t PCKEN25:1; /*!< bit 5: Peripheral clock gating bit */
		uint8_t PCKEN26:1; /*!< bit 6: Peripheral clock gating bit */
		uint8_t PCKEN27:1; /*!< bit 7: Peripheral clock gating bit */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} CLK_PCKENR2_type; /*!< Peripheral clock gating register 2 (CLK_PCKENR2) */

typedef union {
	struct {
		uint8_t CSSEN:1; /*!< bit 0: Clock security system enable */
		const uint8_t AUX:1; /*!< bit 1: Auxiliary oscillator connected to master clock */
		uint8_t CSSDIE:1; /*!< bit 2: Clock security system detection interrupt enable */
		uint8_t CSSD:1; /*!< bit 3: Clock security system detection */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} CLK_CSSR_type; /*!< Clock security system register (CLK_CSSR) */

typedef union {
	struct {
		uint8_t CCOEN:1; /*!< bit 0: Configurable clock output enable */
		uint8_t CCOSEL:4; /*!< bit 1..4: Configurable clock output selection */
		const uint8_t CCORDY:1; /*!< bit 5: Configurable clock output ready */
		const uint8_t CCOBSY:1; /*!< bit 6: Configurable clock output busy */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} CLK_CCOR_type; /*!< Configurable clock output register (CLK_CCOR) */

typedef union {
	struct {
		uint8_t HSITRIM:4; /*!< bit 0..3: HSI trimming value */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} CLK_HSITRIMR_type; /*!< HSI clock calibration trimming register (CLK_HSITRIMR) */

typedef union {
	struct {
		uint8_t SWIMCLK:1; /*!< bit 0: SWIM clock divider */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} CLK_SWIMCCR_type; /*!< SWIM clock control register (CLK_SWIMCCR) */

typedef struct {
	volatile CLK_ICKR_type ICKR;
	volatile CLK_ECKR_type ECKR;
	const uint8_t reserved0[1];
	volatile CLK_CMSR_type CMSR;
	volatile CLK_SWR_type SWR;
	volatile CLK_SWCR_type SWCR;
	volatile CLK_CKDIVR_type CKDIVR;
	volatile CLK_PCKENR1_type PCKENR1;
	volatile CLK_CSSR_type CSSR;
	volatile CLK_CCOR_type CCOR;
	volatile CLK_PCKENR2_type PCKENR2;
	const uint8_t reserved1[1];
	volatile CLK_HSITRIMR_type HSITRIMR;
	volatile CLK_SWIMCCR_type SWIMCCR;
} CLK_type;

#define CLK_BASE 0x50C0
#define CLK ((CLK_type*)CLK_BASE)
#define CLK_ICKR (*(volatile uint8_t *)(CLK_BASE + 0x00))
#define CLK_ICKR_HSIEN (1U << 0)
#define CLK_ICKR_HSIRDY (1U << 1)
#define CLK_ICKR_FHW (1U << 2)
#define CLK_ICKR_LSIEN (1U << 3)
#define CLK_ICKR_LSIRDY (1U << 4)
#define CLK_ICKR_REGAH (1U << 5)
#define CLK_ECKR (*(volatile uint8_t *)(CLK_BASE + 0x01))
#define CLK_ECKR_HSEEN (1U << 0)
#define CLK_ECKR_HSERDY (1U << 1)
#define CLK_CMSR (*(volatile uint8_t *)(CLK_BASE + 0x03))
#define CLK_CMSR_CKM_OFFSET 0
#define CLK_CMSR_CKM_MASK 0xff
#define CLK_CMSR_CKM_HSI 0xe1
#define CLK_CMSR_CKM_LSI 0xd2
#define CLK_CMSR_CKM_HSE 0xb4
#define CLK_SWR (*(volatile uint8_t *)(CLK_BASE + 0x04))
#define CLK_SWR_SWI_OFFSET 0
#define CLK_SWR_SWI_MASK 0xff
#define CLK_SWCR (*(volatile uint8_t *)(CLK_BASE + 0x05))
#define CLK_SWCR_SWBSY (1U << 0)
#define CLK_SWCR_SWEN (1U << 1)
#define CLK_SWCR_SWIEN (1U << 2)
#define CLK_SWCR_SWIF (1U << 3)
#define CLK_CKDIVR (*(volatile uint8_t *)(CLK_BASE + 0x06))
#define CLK_CKDIVR_CPUDIV_OFFSET 0
#define CLK_CKDIVR_CPUDIV_MASK 0x7
#define CLK_CKDIVR_CPUDIV_DIV0 0
#define CLK_CKDIVR_CPUDIV_DIV2 1
#define CLK_CKDIVR_CPUDIV_DIV4 2
#define CLK_CKDIVR_CPUDIV_DIV8 3
#define CLK_CKDIVR_CPUDIV_DIV16 4
#define CLK_CKDIVR_CPUDIV_DIV32 5
#define CLK_CKDIVR_CPUDIV_DIV64 6
#define CLK_CKDIVR_CPUDIV_DIV128 7
#define CLK_CKDIVR_HSIDIV_OFFSET 3
#define CLK_CKDIVR_HSIDIV_MASK 0x3
#define CLK_CKDIVR_HSIDIV_DIV0 0
#define CLK_CKDIVR_HSIDIV_DIV2 1
#define CLK_CKDIVR_HSIDIV_DIV4 2
#define CLK_CKDIVR_HSIDIV_DIV8 3
#define CLK_PCKENR1 (*(volatile uint8_t *)(CLK_BASE + 0x07))
#define CLK_PCKENR1_I2C (1U << 0)
#define CLK_PCKENR1_SPI (1U << 1)
#define CLK_PCKENR1_UART1234 (3U << 2)
#define CLK_PCKENR1_TIM46 (1U << 4)
#define CLK_PCKENR1_TIM25 (1U << 5)
#define CLK_PCKENR1_TIM3 (1U << 6)
#define CLK_PCKENR1_TIM1 (1U << 7)
#define CLK_CSSR (*(volatile uint8_t *)(CLK_BASE + 0x08))
#define CLK_CSSR_CSSEN (1U << 0)
#define CLK_CSSR_AUX (1U << 1)
#define CLK_CSSR_CSSDIE (1U << 2)
#define CLK_CSSR_CSSD (1U << 3)
#define CLK_CCOR (*(volatile uint8_t *)(CLK_BASE + 0x09))
#define CLK_CCOR_CCOEN (1U << 0)
#define CLK_CCOR_CCOSEL_OFFSET 1
#define CLK_CCOR_CCOSEL_MASK 0xf
#define CLK_CCOR_CCOSEL_HSIDIV 0
#define CLK_CCOR_CCOSEL_LSI 1
#define CLK_CCOR_CCOSEL_HSE 2
#define CLK_CCOR_CCOSEL_CPU 4
#define CLK_CCOR_CCOSEL_CPU_DIV2 5
#define CLK_CCOR_CCOSEL_CPU_DIV4 6
#define CLK_CCOR_CCOSEL_CPU_DIV8 7
#define CLK_CCOR_CCOSEL_CPU_DIV16 8
#define CLK_CCOR_CCOSEL_CPU_DIV32 9
#define CLK_CCOR_CCOSEL_CPU_DIV64 10
#define CLK_CCOR_CCOSEL_HSI 11
#define CLK_CCOR_CCOSEL_MASTER 12
#define CLK_CCOR_CCORDY (1U << 5)
#define CLK_CCOR_CCOBSY (1U << 6)
#define CLK_PCKENR2 (*(volatile uint8_t *)(CLK_BASE + 0x0A))
#define CLK_PCKENR2_AWU (1U << 2)
#define CLK_PCKENR2_ADC (1U << 3)
#define CLK_PCKENR2_CAN (1U << 7)
#define CLK_HSITRIMR (*(volatile uint8_t *)(CLK_BASE + 0x0C))
#define CLK_HSITRIMR_OFFSET 0
#define CLK_HSITRIMR_MASK 0xf
#define CLK_SWIMCCR (*(volatile uint8_t *)(CLK_BASE + 0x0D))
#define CLK_SWIMCCR_SWIMCLK (1U << 0)

// Block: WWDG
typedef union {
	struct {
		uint8_t T0:1; /*!< bit 0: 7-bit counter (MSB to LSB) */
		uint8_t T1:1; /*!< bit 1: 7-bit counter (MSB to LSB) */
		uint8_t T2:1; /*!< bit 2: 7-bit counter (MSB to LSB) */
		uint8_t T3:1; /*!< bit 3: 7-bit counter (MSB to LSB) */
		uint8_t T4:1; /*!< bit 4: 7-bit counter (MSB to LSB) */
		uint8_t T5:1; /*!< bit 5: 7-bit counter (MSB to LSB) */
		uint8_t T6:1; /*!< bit 6: 7-bit counter (MSB to LSB) */
		uint8_t WDGA:1; /*!< bit 7: Activation bit */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} WWDG_CR_type; /*!<  */

typedef union {
	struct {
		uint8_t W0:1; /*!< bit 0: 7-bit window value */
		uint8_t W1:1; /*!< bit 1: 7-bit window value */
		uint8_t W2:1; /*!< bit 2: 7-bit window value */
		uint8_t W3:1; /*!< bit 3: 7-bit window value */
		uint8_t W4:1; /*!< bit 4: 7-bit window value */
		uint8_t W5:1; /*!< bit 5: 7-bit window value */
		uint8_t W6:1; /*!< bit 6: 7-bit window value */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} WWDG_WR_type; /*!< Window register (WWDG_WR) */

typedef struct {
	volatile WWDG_CR_type CR;
	volatile WWDG_WR_type WR;
} WWDG_type;

#define WWDG_BASE 0x50D1
#define WWDG ((WWDG_type*)WWDG_BASE)
#define WWDG_CR (*(volatile uint8_t *)(WWDG_BASE + 0x00))
#define WWDG_CR_T_OFFSET 0
#define WWDG_CR_T_MASK 0x7f
#define WWDG_CR_T0 (1U << 0)
#define WWDG_CR_T1 (1U << 1)
#define WWDG_CR_T2 (1U << 2)
#define WWDG_CR_T3 (1U << 3)
#define WWDG_CR_T4 (1U << 4)
#define WWDG_CR_T5 (1U << 5)
#define WWDG_CR_T6 (1U << 6)
#define WWDG_CR_WDGA (1U << 7)
#define WWDG_WR (*(volatile uint8_t *)(WWDG_BASE + 0x01))
#define WWDG_WR_W_OFFSET 0
#define WWDG_WR_W_MASK 0x7f

// Block: IWDG
typedef union {
	struct {
		uint8_t KEY:8; /*!< bit 0..7: Key value */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} IWDG_KR_type; /*!< Key register (IWDG_KR) */

typedef union {
	struct {
		uint8_t PR:3; /*!< bit 0..2: Prescaler divider */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} IWDG_PR_type; /*!< Prescaler register (IWDG_PR) */

typedef union {
	struct {
		uint8_t RL:8; /*!< bit 0..7: Watchdog counter reload value */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} IWDG_RLR_type; /*!< Reload register (IWDG_RLR) */

typedef struct {
	volatile IWDG_KR_type KR;
	volatile IWDG_PR_type PR;
	volatile IWDG_RLR_type RLR;
} IWDG_type;

#define IWDG_BASE 0x50E0
#define IWDG ((IWDG_type*)IWDG_BASE)
#define IWDG_KR (*(volatile uint8_t *)(IWDG_BASE + 0x00))
#define IWDG_KR_KEY_ENABLE 0xCC
#define IWDG_KR_KEY_REFRESH 0xAA
#define IWDG_KR_KEY_ACCESS 0x55
#define IWDG_PR (*(volatile uint8_t *)(IWDG_BASE + 0x01))
#define IWDG_PR_DIV4 0
#define IWDG_PR_DIV8 1
#define IWDG_PR_DIV16 2
#define IWDG_PR_DIV32 3
#define IWDG_PR_DIV64 4
#define IWDG_PR_DIV128 5
#define IWDG_PR_DIV256 6
#define IWDG_RLR (*(volatile uint8_t *)(IWDG_BASE + 0x02))

// Block: AWU
typedef union {
	struct {
		uint8_t MSR:1; /*!< bit 0: Measurement enable */
		uint8_t :3; /*!< bit 1..3: Reserved */
		uint8_t AWUEN:1; /*!< bit 4: Auto-wakeup enable */
		uint8_t AWUF:1; /*!< bit 5: Auto-wakeup flag */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} AWU_CSR_type; /*!< Control/status register (AWU_CSR) */

typedef union {
	struct {
		uint8_t APR:6; /*!< bit 0..5: Asynchronous prescaler divider */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} AWU_APR_type; /*!< Asynchronous prescaler register (AWU_APR) */

typedef union {
	struct {
		uint8_t AWUTB:4; /*!< bit 0..3: Auto-wakeup timebase selection */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} AWU_TBR_type; /*!< Timebase selection register (AWU_TBR) */

typedef struct {
	volatile AWU_CSR_type CSR;
	volatile AWU_APR_type APR;
	volatile AWU_TBR_type TBR;
} AWU_type;

#define AWU_BASE 0x50F0
#define AWU ((AWU_type*)AWU_BASE)
#define AWU_CSR (*(volatile uint8_t *)(AWU_BASE + 0x00))
#define AWU_CSR_MSR (1U << 0)
#define AWU_CSR_AWUEN (1U << 4)
#define AWU_CSR_AWUF (1U << 5)
#define AWU_APR (*(volatile uint8_t *)(AWU_BASE + 0x01))
#define AWU_TBR (*(volatile uint8_t *)(AWU_BASE + 0x02))

// Block: BEEP
typedef union {
	struct {
		uint8_t BEEPDIV:5; /*!< bit 0..4: Beep prescaler divider */
		uint8_t BEEPEN:1; /*!< bit 5: Beep enable */
		uint8_t BEEPSEL:2; /*!< bit 6..7: Beep selection */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} BEEP_CSR_type; /*!< Beeper control/status register (BEEP_CSR) */

typedef struct {
	volatile BEEP_CSR_type CSR;
} BEEP_type;

#define BEEP_BASE 0x50F3
#define BEEP ((BEEP_type*)BEEP_BASE)
#define BEEP_CSR (*(volatile uint8_t *)(BEEP_BASE + 0x00))

// Block: SPI
typedef union {
	struct {
		uint8_t CPHA:1; /*!< bit 0: Clock phase */
		uint8_t CPOL:1; /*!< bit 1: Clock polarity */
		uint8_t MSTR:1; /*!< bit 2: Master selection */
		uint8_t BR:3; /*!< bit 3..5: Baud rate control */
		uint8_t SPE:1; /*!< bit 6: SPI enable */
		uint8_t LSBFIRST:1; /*!< bit 7: Frame format */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} SPI_CR1_type; /*!< SPI control register 1 (SPI_CR1) */

typedef union {
	struct {
		uint8_t SSI:1; /*!< bit 0: Internal slave select */
		uint8_t SSM:1; /*!< bit 1: Software slave management */
		uint8_t RXONLY:1; /*!< bit 2: Receive only */
		uint8_t :1; /*!< bit 3: Reserved */
		uint8_t CRCNEXT:1; /*!< bit 4: Transmit CRC next */
		uint8_t CRCEN:1; /*!< bit 5: Hardware CRC calculation enable */
		uint8_t BDOE:1; /*!< bit 6: Input/Output enable in bidirectional mode */
		uint8_t BDM:1; /*!< bit 7: Bidirectional data mode enable */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} SPI_CR2_type; /*!< SPI control register 2 (SPI_CR2) */

typedef union {
	struct {
		uint8_t :4; /*!< bit 0..3: Reserved */
		uint8_t WKIE:1; /*!< bit 4: Wakeup interrupt enable */
		uint8_t ERRIE:1; /*!< bit 5: Error interrupt enable */
		uint8_t RXIE:1; /*!< bit 6: RX buffer not empty interrupt enable */
		uint8_t TXIE:1; /*!< bit 7: Tx buffer empty interrupt enable */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} SPI_ICR_type; /*!< SPI interrupt control register (SPI_ICR) */

typedef union {
	struct {
		uint8_t RXNE:1; /*!< bit 0: Receive buffer not empty */
		uint8_t TXE:1; /*!< bit 1: Transmit buffer empty */
		uint8_t :1; /*!< bit 2: Reserved */
		uint8_t WKUP:1; /*!< bit 3: Wakeup flag */
		uint8_t CRCERR:1; /*!< bit 4: CRC error flag */
		uint8_t MODF:1; /*!< bit 5: Mode fault */
		uint8_t OVR:1; /*!< bit 6: Overrun flag */
		uint8_t BSY:1; /*!< bit 7: Busy flag */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} SPI_SR_type; /*!< SPI status register (SPI_SR) */

typedef union {
	struct {
		uint8_t DR:8; /*!< bit 0..7: Data register */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} SPI_DR_type; /*!< SPI data register (SPI_DR) */

typedef union {
	struct {
		uint8_t CRCPOLY:8; /*!< bit 0..7: CRC polynomial register */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} SPI_CRCPR_type; /*!< SPI CRC polynomial register (SPI_CRCPR) */

typedef union {
	struct {
		uint8_t RXCRC:8; /*!< bit 0..7: Rx CRC Register */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} SPI_RXCRCR_type; /*!< SPI Rx CRC register (SPI_RXCRCR) */

typedef union {
	struct {
		uint8_t TXCRC:8; /*!< bit 0..7: Tx CRC register */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} SPI_TXCRCR_type; /*!< SPI Tx CRC register (SPI_TXCRCR) */

typedef struct {
	volatile SPI_CR1_type CR1;
	volatile SPI_CR2_type CR2;
	volatile SPI_ICR_type ICR;
	volatile SPI_SR_type SR;
	volatile SPI_DR_type DR;
	volatile SPI_CRCPR_type CRCPR;
	volatile SPI_RXCRCR_type RXCRCR;
	volatile SPI_TXCRCR_type TXCRCR;
} SPI_type;

#define SPI_BASE 0x5200
#define SPI ((SPI_type*)SPI_BASE)
#define SPI_CR1 (*(volatile uint8_t *)(SPI_BASE + 0x00))
#define SPI_CR2 (*(volatile uint8_t *)(SPI_BASE + 0x01))
#define SPI_ICR (*(volatile uint8_t *)(SPI_BASE + 0x02))
#define SPI_SR (*(volatile uint8_t *)(SPI_BASE + 0x03))
#define SPI_DR (*(volatile uint8_t *)(SPI_BASE + 0x04))
#define SPI_CRCPR (*(volatile uint8_t *)(SPI_BASE + 0x05))
#define SPI_RXCRCR (*(volatile uint8_t *)(SPI_BASE + 0x06))
#define SPI_TXCRCR (*(volatile uint8_t *)(SPI_BASE + 0x07))

// Block: I2C
typedef union {
	struct {
		uint8_t PE:1; /*!< bit 0: Peripheral enable */
		uint8_t :5; /*!< bit 1..5: Reserved */
		uint8_t ENGC:1; /*!< bit 6: General call enable */
		uint8_t NOSTRETCH:1; /*!< bit 7: Clock stretching disable (Slave mode) */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} I2C_CR1_type; /*!< Control register 1 (I2C_CR1) */

typedef union {
	struct {
		uint8_t START:1; /*!< bit 0: Start generation */
		uint8_t STOP:1; /*!< bit 1: Stop generation */
		uint8_t ACK:1; /*!< bit 2: Acknowledge enable */
		uint8_t POS:1; /*!< bit 3: Acknowledge position (for data reception) */
		uint8_t :3; /*!< bit 4..6: Reserved */
		uint8_t SWRST:1; /*!< bit 7: Software reset */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} I2C_CR2_type; /*!< Control register 2 (I2C_CR2) */

typedef union {
	struct {
		uint8_t FREQ:6; /*!< bit 0..5: Peripheral clock frequency */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} I2C_FREQR_type; /*!< Frequency register (I2C_FREQR) */

typedef union {
	struct {
		uint8_t ADD0:1; /*!< bit 0: Interface address */
		uint8_t ADD:7; /*!< bit 1..7: Interface address */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} I2C_OARL_type; /*!< Own address register LSB (I2C_OARL) */

typedef union {
	struct {
		uint8_t :1; /*!< bit 0: Reserved */
		uint8_t ADD:2; /*!< bit 1..2: Interface address */
		uint8_t :3; /*!< bit 3..5: Reserved */
		uint8_t ADDCONF:1; /*!< bit 6: Address mode configuration */
		uint8_t ADDMODE:1; /*!< bit 7: Addressing mode (Slave mode) */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} I2C_OARH_type; /*!< Own address register MSB (I2C_OARH) */

typedef union {
	struct {
		uint8_t DR:8; /*!< bit 0..7: Data register */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} I2C_DR_type; /*!< Data register (I2C_DR) */

typedef union {
	struct {
		uint8_t SB:1; /*!< bit 0: Start bit (Master mode) */
		uint8_t ADDR:1; /*!< bit 1: Address sent (master mode)/matched (slave mode) */
		uint8_t BTF:1; /*!< bit 2: Byte transfer finished */
		uint8_t ADD10:1; /*!< bit 3: 10-bit header sent (Master mode) */
		uint8_t STOPF:1; /*!< bit 4: Stop detection (Slave mode) */
		uint8_t :1; /*!< bit 5: Reserved */
		uint8_t RXNE:1; /*!< bit 6: Data register not empty (receivers) */
		uint8_t TXE:1; /*!< bit 7: Data register empty (transmitters) */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} I2C_SR1_type; /*!< Status register 1 (I2C_SR1) */

typedef union {
	struct {
		uint8_t BERR:1; /*!< bit 0: Bus error */
		uint8_t ARLO:1; /*!< bit 1: Arbitration lost (master mode) */
		uint8_t AF:1; /*!< bit 2: Acknowledge failure */
		uint8_t OVR:1; /*!< bit 3: Overrun/underrun */
		uint8_t :1; /*!< bit 4: Reserved */
		uint8_t WUFH:1; /*!< bit 5: Wakeup from Halt */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} I2C_SR2_type; /*!< Status register 2 (I2C_SR2) */

typedef union {
	struct {
		uint8_t MSL:1; /*!< bit 0: Master/Slave */
		uint8_t BUSY:1; /*!< bit 1: Bus busy */
		uint8_t TRA:1; /*!< bit 2: Transmitter/Receiver */
		uint8_t :1; /*!< bit 3: Reserved */
		uint8_t GENCALL:1; /*!< bit 4: General call header (Slave mode) */
		uint8_t :2; /*!< bit 5..6: Reserved */
		uint8_t DUALF:1; /*!< bit 7: Dual flag (Slave mode) */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} I2C_SR3_type; /*!< Status register 3 (I2C_SR3) */

typedef union {
	struct {
		uint8_t ITERREN:1; /*!< bit 0: Error interrupt enable */
		uint8_t ITEVTEN:1; /*!< bit 1: Event interrupt enable */
		uint8_t ITBUFEN:1; /*!< bit 2: Buffer interrupt enable */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} I2C_ITR_type; /*!< Interrupt register (I2C_ITR) */

typedef union {
	struct {
		uint8_t CCR7_0:8; /*!< bit 0..7: Clock control register (Master mode) */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} I2C_CCRL_type; /*!< Clock control register low (I2C_CCRL) */

typedef union {
	struct {
		uint8_t CCR11_8:4; /*!< bit 0..3: Clock control register in Fast/Standard mode (Master mode) */
		uint8_t :2; /*!< bit 4..5: Reserved */
		uint8_t DUTY:1; /*!< bit 6: Fast mode duty cycle */
		uint8_t FS:1; /*!< bit 7: I2C master mode selection */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} I2C_CCRH_type; /*!< Clock control register high (I2C_CCRH) */

typedef union {
	struct {
		uint8_t TRISE:6; /*!< bit 0..5: Maximum rise time in Fast/Standard mode (Master mode) */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} I2C_TRISER_type; /*!< TRISE register (I2C_TRISER) */

typedef struct {
	volatile I2C_CR1_type CR1;
	volatile I2C_CR2_type CR2;
	volatile I2C_FREQR_type FREQR;
	volatile I2C_OARL_type OARL;
	volatile I2C_OARH_type OARH;
	const uint8_t reserved[1];
	volatile I2C_DR_type DR;
	volatile I2C_SR1_type SR1;
	volatile I2C_SR2_type SR2;
	volatile I2C_SR3_type SR3;
	volatile I2C_ITR_type ITR;
	volatile I2C_CCRL_type CCRL;
	volatile I2C_CCRH_type CCRH;
	volatile I2C_TRISER_type TRISER;
} I2C_type;

#define I2C_BASE 0x5210
#define I2C ((I2C_type*)I2C_BASE)
#define I2C_CR1 (*(volatile uint8_t *)(I2C_BASE + 0x00))
#define I2C_CR1_PE (1U << 0)
#define I2C_CR1_ENGC (1U << 6)
#define I2C_CR1_NOSTRETCH (1U << 7)
#define I2C_CR2 (*(volatile uint8_t *)(I2C_BASE + 0x01))
#define I2C_CR2_START (1U << 0)
#define I2C_CR2_STOP (1U << 1)
#define I2C_CR2_ACK (1U << 2)
#define I2C_CR2_POS (1U << 3)
#define I2C_CR2_SWRST (1U << 7)
#define I2C_FREQR (*(volatile uint8_t *)(I2C_BASE + 0x02))
#define I2C_OARL (*(volatile uint8_t *)(I2C_BASE + 0x03))
#define I2C_OARL_ARR0 (1U << 0)
#define I2C_OARH (*(volatile uint8_t *)(I2C_BASE + 0x04))
#define I2C_OARH_ADDCONF (1U << 6)
#define I2C_OARH_ADDMODE (1U << 7)
#define I2C_DR (*(volatile uint8_t *)(I2C_BASE + 0x06))
#define I2C_SR1 (*(volatile uint8_t *)(I2C_BASE + 0x07))
#define I2C_SR1_SB (1U << 0)
#define I2C_SR1_ADDR (1U << 1)
#define I2C_SR1_BTF (1U << 2)
#define I2C_SR1_ADD10 (1U << 3)
#define I2C_SR1_STOPF (1U << 4)
#define I2C_SR1_RXNE (1U << 6)
#define I2C_SR1_TXE (1U << 7)
#define I2C_SR2 (*(volatile uint8_t *)(I2C_BASE + 0x08))
#define I2C_SR2_BERR (1U << 0)
#define I2C_SR2_ARLO (1U << 1)
#define I2C_SR2_AF (1U << 2)
#define I2C_SR2_OVR (1U << 3)
#define I2C_SR2_WUFH (1U << 5)
#define I2C_SR3 (*(volatile uint8_t *)(I2C_BASE + 0x09))
#define I2C_SR3_MSL (1U << 0)
#define I2C_SR3_BUSY (1U << 1)
#define I2C_SR3_TRA (1U << 2)
#define I2C_SR3_GENCALL (1U << 4)
#define I2C_SR3_DUALF (1U << 7)
#define I2C_ITR (*(volatile uint8_t *)(I2C_BASE + 0x0A))
#define I2C_ITR_ITERREN (1U << 0)
#define I2C_ITR_ITEVTEN (1U << 1)
#define I2C_ITR_ITBUFEN (1U << 2)
#define I2C_CCRL (*(volatile uint8_t *)(I2C_BASE + 0x0B))
#define I2C_CCRH (*(volatile uint8_t *)(I2C_BASE + 0x0C))
#define I2C_CCRH_DUTY (1U << 6)
#define I2C_CCRH_FS (1U << 7)
#define I2C_TRISER (*(volatile uint8_t *)(I2C_BASE + 0x0D))
#define I2C_PECR (*(volatile uint8_t *)(I2C_BASE + 0x0E))

// Block: UART
typedef union {
	struct {
		uint8_t PE:1; /*!< bit 0: Parity error */
		uint8_t FE:1; /*!< bit 1: Framing error */
		uint8_t NF:1; /*!< bit 2: Noise flag */
		uint8_t LHE_OR:1; /*!< bit 3: LIN Header Error (LIN slave mode)/Overrun error */
		uint8_t IDLE:1; /*!< bit 4: IDLE line detected */
		uint8_t RXNE:1; /*!< bit 5: Read data register not empty */
		uint8_t TC:1; /*!< bit 6: Transmission complete */
		uint8_t TXE:1; /*!< bit 7: Transmit data register empty */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} UART_SR_type; /*!< Status register (UART_SR) */

typedef union {
	struct {
		uint8_t DR:8; /*!< bit 0..7: Data value */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} UART_DR_type; /*!< Data register (UART_DR) */

typedef union {
	struct {
		uint8_t UART_DIV:8; /*!< bit 0..7: UART_DIV bits */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} UART_BRR1_type; /*!< Baud rate register 1 (UART_BRR1) */

typedef union {
	struct {
		uint8_t UART_DIV_LSB:4; /*!< bit 0..3: LSB of UART_DIV */
		uint8_t UART_DIV_MSB:4; /*!< bit 4..7: MSB of UART_DIV */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} UART_BRR2_type; /*!< Baud rate register 2 (UART_BRR2) */

typedef union {
	struct {
		uint8_t PIEN:1; /*!< bit 0: Parity interrupt enable */
		uint8_t PS:1; /*!< bit 1: Parity selection */
		uint8_t PCEN:1; /*!< bit 2: Parity control enable */
		uint8_t WAKE:1; /*!< bit 3: Wakeup method */
		uint8_t M:1; /*!< bit 4: Word length */
		uint8_t UARTD:1; /*!< bit 5: UART Disable (for low power consumption) */
		uint8_t T8:1; /*!< bit 6: Transmit data bit 8 */
		uint8_t R8:1; /*!< bit 7: Receive Data bit 8 */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} UART_CR1_type; /*!< Control register 1 (UART_CR1) */

typedef union {
	struct {
		uint8_t SBK:1; /*!< bit 0: Send break */
		uint8_t RWU:1; /*!< bit 1: Receiver wakeup */
		uint8_t REN:1; /*!< bit 2: Receiver enable */
		uint8_t TEN:1; /*!< bit 3: Transmitter enable */
		uint8_t ILIEN:1; /*!< bit 4: IDLE Line interrupt enable */
		uint8_t RIEN:1; /*!< bit 5: Receiver interrupt enable */
		uint8_t TCIEN:1; /*!< bit 6: Transmission complete interrupt enable */
		uint8_t TIEN:1; /*!< bit 7: Transmitter interrupt enable */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} UART_CR2_type; /*!< Control register 2 (UART_CR2) */

typedef union {
	struct {
		uint8_t LBCL:1; /*!< bit 0: Last bit clock pulse */
		uint8_t CPHA:1; /*!< bit 1: Clock phase */
		uint8_t CPOL:1; /*!< bit 2: Clock polarity */
		uint8_t CLKEN:1; /*!< bit 3: Clock enable */
		uint8_t STOP:2; /*!< bit 4..5: STOP bits */
		uint8_t LINEN:1; /*!< bit 6: LIN mode enable */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} UART_CR3_type; /*!< Control register 3 (UART_CR3) */

typedef union {
	struct {
		uint8_t ADD:4; /*!< bit 0..3: Address of the UART node */
		uint8_t LBDF:1; /*!< bit 4: LIN Break Detection Flag */
		uint8_t LBDL:1; /*!< bit 5: LIN Break Detection Length */
		uint8_t LBDIEN:1; /*!< bit 6: LIN Break Detection Interrupt Enable */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} UART_CR4_type; /*!< Control register 4 (UART_CR4) */

typedef union {
	struct {
		uint8_t :1; /*!< bit 0: Reserved */
		uint8_t IREN:1; /*!< bit 1: IrDA mode Enable */
		uint8_t IRLP:1; /*!< bit 2: IrDA Low Power */
		uint8_t HDSEL:1; /*!< bit 3: Half-Duplex Selection */
		uint8_t NACK:1; /*!< bit 4: Smartcard NACK enable */
		uint8_t SCEN:1; /*!< bit 5: Smartcard mode enable */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} UART_CR5_type; /*!< Control register 5 (UART_CR5) */

typedef union {
	struct {
		uint8_t LSF:1; /*!< bit 0: LIN Sync Field */
		uint8_t LHDF:1; /*!< bit 1: LIN Header Detection Flag */
		uint8_t LHDIEN:1; /*!< bit 2: LIN Header Detection Interrupt Enable */
		uint8_t :1; /*!< bit 3: Reserved */
		uint8_t LASE:1; /*!< bit 4: LIN automatic resynchronisation enable */
		uint8_t LSLV:1; /*!< bit 5: LIN Slave Enable */
		uint8_t :1; /*!< bit 6: Reserved */
		uint8_t LDUM:1; /*!< bit 7: LIN Divider Update Method */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} UART_CR6_type; /*!< Control register 6 (UART_CR6) */

typedef union {
	struct {
		uint8_t GT:8; /*!< bit 0..7: Guard time value */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} UART_GTR_type; /*!< Guard time register (UART_GTR) */

typedef union {
	struct {
		uint8_t PSC:8; /*!< bit 0..7: Prescaler value */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} UART_PSCR_type; /*!< Prescaler register (UART_PSCR) */

// Block: UART1
typedef struct {
	volatile UART_SR_type SR;
	volatile UART_DR_type DR;
	volatile UART_BRR1_type BRR1;
	volatile UART_BRR2_type BRR2;
	volatile UART_CR1_type CR1;
	volatile UART_CR2_type CR2;
	volatile UART_CR3_type CR3;
	volatile UART_CR4_type CR4;
	volatile UART_CR5_type CR5;
	volatile UART_GTR_type GTR;
	volatile UART_PSCR_type PSCR;
} UART1_type;
#define UART1_BASE 0x5230
#define UART1 ((UART1_type*)UART1_BASE)
#define UART1_SR (*(volatile uint8_t *)(UART1_BASE + 0x00))
#define UART1_DR (*(volatile uint8_t *)(UART1_BASE + 0x01))
#define UART1_BRR1 (*(volatile uint8_t *)(UART1_BASE + 0x02))
#define UART1_BRR2 (*(volatile uint8_t *)(UART1_BASE + 0x03))
#define UART1_CR1 (*(volatile uint8_t *)(UART1_BASE + 0x04))
#define UART1_CR2 (*(volatile uint8_t *)(UART1_BASE + 0x05))
#define UART1_CR3 (*(volatile uint8_t *)(UART1_BASE + 0x06))
#define UART1_CR4 (*(volatile uint8_t *)(UART1_BASE + 0x07))
#define UART1_CR5 (*(volatile uint8_t *)(UART1_BASE + 0x08))
#define UART1_GTR (*(volatile uint8_t *)(UART1_BASE + 0x09))
#define UART1_PSCR (*(volatile uint8_t *)(UART1_BASE + 0x0A))

// Block: UART2
typedef struct {
	volatile UART_SR_type SR;
	volatile UART_DR_type DR;
	volatile UART_BRR1_type BRR1;
	volatile UART_BRR2_type BRR2;
	volatile UART_CR1_type CR1;
	volatile UART_CR2_type CR2;
	volatile UART_CR3_type CR3;
	volatile UART_CR4_type CR4;
	volatile UART_CR5_type CR5;
	volatile UART_CR6_type CR6;
	volatile UART_GTR_type GTR;
	volatile UART_PSCR_type PSCR;
} UART24_type;
#define UART2_BASE 0xFFFF
#define UART2 ((UART24_type*)UART2_BASE)
#define UART2_SR (*(volatile uint8_t *)(UART2_BASE + 0x00))
#define UART2_DR (*(volatile uint8_t *)(UART2_BASE + 0x01))
#define UART2_BRR1 (*(volatile uint8_t *)(UART2_BASE + 0x02))
#define UART2_BRR2 (*(volatile uint8_t *)(UART2_BASE + 0x03))
#define UART2_CR1 (*(volatile uint8_t *)(UART2_BASE + 0x04))
#define UART2_CR2 (*(volatile uint8_t *)(UART2_BASE + 0x05))
#define UART2_CR3 (*(volatile uint8_t *)(UART2_BASE + 0x06))
#define UART2_CR4 (*(volatile uint8_t *)(UART2_BASE + 0x07))
#define UART2_CR5 (*(volatile uint8_t *)(UART2_BASE + 0x08))
#define UART2_CR6 (*(volatile uint8_t *)(UART2_BASE + 0x09))
#define UART2_GTR (*(volatile uint8_t *)(UART2_BASE + 0x0A))
#define UART2_PSCR (*(volatile uint8_t *)(UART2_BASE + 0x0B))

// Block: UART3
typedef struct {
	volatile UART_SR_type SR;
	volatile UART_DR_type DR;
	volatile UART_BRR1_type BRR1;
	volatile UART_BRR2_type BRR2;
	volatile UART_CR1_type CR1;
	volatile UART_CR2_type CR2;
	volatile UART_CR3_type CR3;
	volatile UART_CR4_type CR4;
	const uint8_t reserved[1];
	volatile UART_CR6_type CR6;
} UART3_type;
#define UART3_BASE 0xFFFF
#define UART3 ((UART3_type*)UART3_BASE)
#define UART3_SR (*(volatile uint8_t *)(UART3_BASE + 0x00))
#define UART3_DR (*(volatile uint8_t *)(UART3_BASE + 0x01))
#define UART3_BRR1 (*(volatile uint8_t *)(UART3_BASE + 0x02))
#define UART3_BRR2 (*(volatile uint8_t *)(UART3_BASE + 0x03))
#define UART3_CR1 (*(volatile uint8_t *)(UART3_BASE + 0x04))
#define UART3_CR2 (*(volatile uint8_t *)(UART3_BASE + 0x05))
#define UART3_CR3 (*(volatile uint8_t *)(UART3_BASE + 0x06))
#define UART3_CR4 (*(volatile uint8_t *)(UART3_BASE + 0x07))
#define UART3_CR6 (*(volatile uint8_t *)(UART3_BASE + 0x09))

// Block: UART4
#define UART4_BASE 0xFFFF
#define UART4 ((UART24_type*)UART4_BASE)
#define UART4_SR (*(volatile uint8_t *)(UART4_BASE + 0x00))
#define UART4_DR (*(volatile uint8_t *)(UART4_BASE + 0x01))
#define UART4_BRR1 (*(volatile uint8_t *)(UART4_BASE + 0x02))
#define UART4_BRR2 (*(volatile uint8_t *)(UART4_BASE + 0x03))
#define UART4_CR1 (*(volatile uint8_t *)(UART4_BASE + 0x04))
#define UART4_CR2 (*(volatile uint8_t *)(UART4_BASE + 0x05))
#define UART4_CR3 (*(volatile uint8_t *)(UART4_BASE + 0x06))
#define UART4_CR4 (*(volatile uint8_t *)(UART4_BASE + 0x07))
#define UART4_CR5 (*(volatile uint8_t *)(UART4_BASE + 0x08))
#define UART4_CR6 (*(volatile uint8_t *)(UART4_BASE + 0x09))
#define UART4_GTR (*(volatile uint8_t *)(UART4_BASE + 0x0A))
#define UART4_PSCR (*(volatile uint8_t *)(UART4_BASE + 0x0B))

// Block: CAN
typedef union {
	struct {
		uint8_t INRQ:1; /*!< bit 0: Initialization Request */
		uint8_t SLEEP:1; /*!< bit 1: Sleep Mode Request */
		uint8_t TXFP:1; /*!< bit 2: Transmit FIFO Priority */
		uint8_t RFLM:1; /*!< bit 3: Receive FIFO Locked Mode */
		uint8_t NART:1; /*!< bit 4: No Automatic Retransmission */
		uint8_t AWUM:1; /*!< bit 5: Automatic wakeup Mode */
		uint8_t ABOM:1; /*!< bit 6: Automatic Bus-Off Management */
		uint8_t TTCM:1; /*!< bit 7: Time Triggered Communication Mode */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} CAN_MCR_type; /*!< CAN master control register (CAN_MCR) */

typedef union {
	struct {
		uint8_t INAK:1; /*!< bit 0: Initialization Acknowledge */
		uint8_t SLAK:1; /*!< bit 1: Sleep Acknowledge */
		uint8_t ERRI:1; /*!< bit 2: Error Interrupt */
		uint8_t WKUI:1; /*!< bit 3: Wakeup Interrupt */
		uint8_t TX:1; /*!< bit 4: Transmit */
		uint8_t RX:1; /*!< bit 5: Receive */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} CAN_MSR_type; /*!< CAN master status register (CAN_MSR) */

typedef union {
	struct {
		uint8_t RQCP0:1; /*!< bit 0: Request Completed for Mailbox 0 */
		uint8_t RQCP1:1; /*!< bit 1: Request Completed for Mailbox 1 */
		uint8_t RQCP2:1; /*!< bit 2: Request Completed for Mailbox 2 */
		uint8_t :1; /*!< bit 3: Reserved */
		const uint8_t TXOK0:1; /*!< bit 4: Transmission OK for mailbox 0 */
		const uint8_t TXOK1:1; /*!< bit 5: Transmission OK for mailbox 1 */
		const uint8_t TXOK2:1; /*!< bit 6: Transmission OK for mailbox 2 */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} CAN_TSR_type; /*!< CAN transmit status register (CAN_TSR) */

typedef union {
	struct {
		const uint8_t CODE:2; /*!< bit 0..1: Mailbox Code */
		const uint8_t TME0:1; /*!< bit 2: Transmit Mailbox 0 Empty */
		const uint8_t TME1:1; /*!< bit 3: Transmit Mailbox 1 Empty */
		const uint8_t TME2:1; /*!< bit 4: Transmit Mailbox 2 Empty */
		const uint8_t LOW0:1; /*!< bit 5: Lowest Priority Flag for Mailbox 0 */
		const uint8_t LOW1:1; /*!< bit 6: Lowest Priority Flag for Mailbox 1 */
		const uint8_t LOW2:1; /*!< bit 7: Lowest Priority Flag for Mailbox 2 */
	} fields; /*!< structure used for bit access */
	const uint8_t reg; /*!< type used for register access */
} CAN_TPR_type; /*!< CAN transmit priority register (CAN_TPR) */

typedef union {
	struct {
		const uint8_t FMP:2; /*!< bit 0..1: FIFO Message Pending */
		uint8_t :1; /*!< bit 2: Reserved */
		uint8_t FULL:1; /*!< bit 3: FIFO Full */
		uint8_t FOVR:1; /*!< bit 4: FIFO Overrun */
		uint8_t RFOM:1; /*!< bit 5: Release FIFO Output Mailbox */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} CAN_RFR_type; /*!< CAN receive FIFO register (CAN_RFR) */

typedef union {
	struct {
		uint8_t TMEIE:1; /*!< bit 0: Transmit Mailbox Empty Interrupt Enable */
		uint8_t FMPIE:1; /*!< bit 1: FIFO Message Pending Interrupt Enable */
		uint8_t FFIE:1; /*!< bit 2: FIFO Full Interrupt Enable */
		uint8_t FOVIE:1; /*!< bit 3: FIFO Overrun Interrupt Enable */
		uint8_t :3; /*!< bit 4..6: Reserved */
		uint8_t WKUIE:1; /*!< bit 7: Wakeup Interrupt Enable */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} CAN_IER_type; /*!< CAN interrupt enable register (CAN_IER) */

typedef union {
	struct {
		uint8_t LBKM:1; /*!< bit 0: Loop back mode */
		uint8_t SILM:1; /*!< bit 1: Silent mode */
		const uint8_t SAMP:1; /*!< bit 2: Last sample point */
		const uint8_t RX:1; /*!< bit 3: CAN Rx Signal */
		uint8_t TXM2E:1; /*!< bit 4: TX Mailbox 2 enable */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} CAN_DGR_type; /*!< CAN diagnostic register (CAN_DGR) */

typedef union {
	struct {
		uint8_t PS:3; /*!< bit 0..2: Page select */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} CAN_PSR_type; /*!< CAN page select register (CAN_PSR) */

typedef union {
	struct {
		const uint8_t EWGF:1; /*!< bit 0: Error warning flag */
		const uint8_t EPVF:1; /*!< bit 1: Error passive flag */
		const uint8_t BOFF:1; /*!< bit 2: Bus-off flag */
		uint8_t :1; /*!< bit 3: Reserved */
		uint8_t LEC:3; /*!< bit 4..6: Last error code */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} CAN_ESR_type; /*!< CAN error status register (CAN_ESR) */

typedef union {
	struct {
		uint8_t EWGIE:1; /*!< bit 0: Error warning interrupt enable */
		uint8_t EPVIE:1; /*!< bit 1: Error passive interrupt enable */
		uint8_t BOFIE:1; /*!< bit 2: Bus-Off interrupt enable */
		uint8_t :1; /*!< bit 3: Reserved */
		uint8_t LECIE:1; /*!< bit 4: Last error code interrupt enable */
		uint8_t :2; /*!< bit 5..6: Reserved */
		uint8_t ERRIE:1; /*!< bit 7: Error interrupt enable */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} CAN_EIER_type; /*!< CAN error interrupt enable register (CAN_EIER) */

typedef union {
	struct {
		const uint8_t TEC:8; /*!< bit 0..7: Transmit error counter */
	} fields; /*!< structure used for bit access */
	const uint8_t reg; /*!< type used for register access */
} CAN_TECR_type; /*!< CAN transmit error counter register (CAN_TECR) */

typedef union {
	struct {
		const uint8_t REC:8; /*!< bit 0..7: Receive error counter */
	} fields; /*!< structure used for bit access */
	const uint8_t reg; /*!< type used for register access */
} CAN_RECR_type; /*!< CAN receive error counter register (CAN_RECR) */

typedef union {
	struct {
		uint8_t BRP:6; /*!< bit 0..5: Baud rate prescaler */
		uint8_t SJW:2; /*!< bit 6..7: Resynchronization jump width */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} CAN_BTR1_type; /*!< CAN bit timing register 1 (CAN_BTR1) */

typedef union {
	struct {
		uint8_t BS1:4; /*!< bit 0..3: Bit Segment 1 */
		uint8_t BS2:3; /*!< bit 4..6: Bit Segment 2 */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} CAN_BTR2_type; /*!< CAN bit timing register 2 (CAN_BTR2) */

typedef union {
	struct {
		uint8_t TXRQ:1; /*!< bit 0: Transmit mailbox request */
		uint8_t ABRQ:1; /*!< bit 1: Abort request for mailbox */
		uint8_t RQCP:1; /*!< bit 2: Request completed */
		const uint8_t TXOK:1; /*!< bit 3: Transmission OK */
		const uint8_t ALST:1; /*!< bit 4: Arbitration lost */
		const uint8_t TERR:1; /*!< bit 5: Transmission error */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} CAN_MCSR_type; /*!< CAN message control/status register (CAN_MCSR) */

typedef union {
	struct {
		uint8_t FMI:8; /*!< bit 0..7: Filter match index */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} CAN_MFMIR_type; /*!< CAN mailbox filter match index register (CAN_MFMIR) */

typedef union {
	struct {
		union {
			uint8_t STID10_6:5; /*!< bit 0..4: Standard identifier */
			uint8_t EXID28_24:5; /*!< bit 0..4: Extended identifier */
		};
		uint8_t RTR:1; /*!< bit 5: Remote transmission request */
		uint8_t IDE:1; /*!< bit 6: Extended identifier */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} CAN_MIDR1_type; /*!< CAN mailbox identifier register 1 (CAN_MIDR1) */

typedef union {
	struct {
		uint8_t EXID17_16:2; /*!< bit 0..1: Extended Identifier */
		union {
			uint8_t STID5_0:6; /*!< bit 2..7: Standard Identifier */
			uint8_t EXID23_18:6; /*!< bit 2..7: Extended Identifier */
		};
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} CAN_MIDR2_type; /*!< CAN mailbox identifier register 2 (CAN_MIDR2) */

typedef union {
	struct {
		uint8_t EXID15_8:8; /*!< bit 0..7: EXID */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} CAN_MIDR3_type; /*!< CAN mailbox identifier register 3 (CAN_MIDR3) */

typedef union {
	struct {
		uint8_t EXID7_0:8; /*!< bit 0..7: EXID */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} CAN_MIDR4_type; /*!< CAN mailbox identifier register 4 (CAN_MIDR4) */

typedef union {
	struct {
		uint8_t DLC:4; /*!< bit 0..3: Data length code */
		uint8_t :3; /*!< bit 4..6: Reserved */
		uint8_t TGT:1; /*!< bit 7: Transmit global time */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} CAN_MDLCR_type; /*!< CAN mailbox data length control register (CAN_MDLCR) */

typedef union {
	struct {
		uint8_t DATA:8; /*!< bit 0..7: Data */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} CAN_MDAR_type; /*!< CAN mailbox data register x (CAN_MDAR) (x= 1 .. 8) */

typedef union {
	struct {
		uint8_t TIME:8; /*!< bit 0..7: Message time stamp low */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} CAN_MTSRL_type; /*!< CAN mailbox time stamp register low (CAN_MTSRL) */

typedef union {
	struct {
		uint8_t TIME:8; /*!< bit 0..7: Message time stamp high */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} CAN_MTSRH_type; /*!< CAN mailbox time stamp register high (CAN_MTSRH) */

typedef union {
	struct {
		uint8_t FML0:1; /*!< bit 0: Filter 0 mode low */
		uint8_t FMH0:1; /*!< bit 1: Filter 0 mode high */
		uint8_t FML1:1; /*!< bit 2: Filter 1 mode low */
		uint8_t FMH1:1; /*!< bit 3: Filter 1 mode high */
		uint8_t FML2:1; /*!< bit 4: Filter 2 mode low */
		uint8_t FMH2:1; /*!< bit 5: Filter 2 mode high */
		uint8_t FML3:1; /*!< bit 6: Filter 3 mode low */
		uint8_t FMH3:1; /*!< bit 7: Filter 3 mode high */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} CAN_FMR1_type; /*!< CAN filter mode register 1 (CAN_FMR1) */

typedef union {
	struct {
		uint8_t FML4:1; /*!< bit 0: Filter 4 mode low */
		uint8_t FMH4:1; /*!< bit 1: Filter 4 mode high */
		uint8_t FML5:1; /*!< bit 2: Filter 5 mode low */
		uint8_t FMH5:1; /*!< bit 3: Filter 5 mode high */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} CAN_FMR2_type; /*!< CAN filter mode register 2 (CAN_FMR2) */

typedef union {
	struct {
		uint8_t FACT0:1; /*!< bit 0: Filter active */
		uint8_t FSC0:2; /*!< bit 1..2: Filter scale configuration */
		uint8_t :1; /*!< bit 3: Reserved */
		uint8_t FACT1:1; /*!< bit 4: Filter Active */
		uint8_t FSC1:2; /*!< bit 5..6: Filter scale configuration */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} CAN_FCR1_type; /*!< CAN filter configuration register 1 (CAN_FCR1) */

typedef union {
	struct {
		uint8_t FACT2:1; /*!< bit 0: Filter active */
		uint8_t FSC2:2; /*!< bit 1..2: Filter scale configuration */
		uint8_t :1; /*!< bit 3: Reserved */
		uint8_t FACT3:1; /*!< bit 4: Filter Active */
		uint8_t FSC3:2; /*!< bit 5..6: Filter scale configuration */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} CAN_FCR2_type; /*!< CAN filter configuration register 2 (CAN_FCR2) */

typedef union {
	struct {
		uint8_t FACT4:1; /*!< bit 0: Filter active */
		uint8_t FSC4:2; /*!< bit 1..2: Filter scale configuration */
		uint8_t :1; /*!< bit 3: Reserved */
		uint8_t FACT5:1; /*!< bit 4: Filter Active */
		uint8_t FSC5:2; /*!< bit 5..6: Filter scale configuration */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} CAN_FCR3_type; /*!< CAN filter configuration register 1 (CAN_FCR3) */

typedef union {
	struct {
		uint8_t FB:8; /*!< bit 0..7: Filter bits */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} CAN_FiRx_type; /*!< CAN filter bank i register x (CAN_FiRx) (i = 0 .. 5, x = 1 .. 8) */

typedef struct {
	union {
		volatile CAN_MFMIR_type MFMIR;
		volatile CAN_MCSR_type MCSR;
	};
	volatile CAN_MDLCR_type MDLCR;
	volatile CAN_MIDR1_type MIDR1;
	volatile CAN_MIDR2_type MIDR2;
	volatile CAN_MIDR3_type MIDR3;
	volatile CAN_MIDR4_type MIDR4;
	volatile CAN_MDAR_type MDAR1;
	volatile CAN_MDAR_type MDAR2;
	volatile CAN_MDAR_type MDAR3;
	volatile CAN_MDAR_type MDAR4;
	volatile CAN_MDAR_type MDAR5;
	volatile CAN_MDAR_type MDAR6;
	volatile CAN_MDAR_type MDAR7;
	volatile CAN_MDAR_type MDAR8;
	volatile CAN_MTSRL_type MTSRL;
	volatile CAN_MTSRH_type MTSRH;
} CAN_MAILBOX_type;

typedef struct {
	volatile CAN_ESR_type ESR;
	volatile CAN_EIER_type EIER;
	volatile CAN_TECR_type TECR;
	volatile CAN_RECR_type RECR;
	volatile CAN_BTR1_type BTR1;
	volatile CAN_BTR2_type BTR2;
	const uint8_t reserved[2];
	volatile CAN_FMR1_type FMR1;
	volatile CAN_FMR2_type FMR2;
	volatile CAN_FCR1_type FCR1;
	volatile CAN_FCR2_type FCR2;
	volatile CAN_FCR3_type FCR3;
} CAN_CONFIGURATION_type;

typedef struct {
	volatile CAN_MCR_type MCR;
	volatile CAN_MSR_type MSR;
	volatile CAN_TSR_type TSR;
	volatile CAN_TPR_type TPR;
	volatile CAN_RFR_type RFR;
	volatile CAN_IER_type IER;
	volatile CAN_DGR_type DGR;
	volatile CAN_PSR_type PSR;
	union {
		CAN_MAILBOX_type TX_MAILBOX_0;
		CAN_MAILBOX_type TX_MAILBOX_1;
		struct {
			volatile CAN_FiRx_type F0R1;
			volatile CAN_FiRx_type F0R2;
			volatile CAN_FiRx_type F0R3;
			volatile CAN_FiRx_type F0R4;
			volatile CAN_FiRx_type F0R5;
			volatile CAN_FiRx_type F0R6;
			volatile CAN_FiRx_type F0R7;
			volatile CAN_FiRx_type F0R8;
			volatile CAN_FiRx_type F1R1;
			volatile CAN_FiRx_type F1R2;
			volatile CAN_FiRx_type F1R3;
			volatile CAN_FiRx_type F1R4;
			volatile CAN_FiRx_type F1R5;
			volatile CAN_FiRx_type F1R6;
			volatile CAN_FiRx_type F1R7;
			volatile CAN_FiRx_type F1R8;
		} ACCEPTANCE_FILTER_01;
		struct {
			volatile CAN_FiRx_type F2R1;
			volatile CAN_FiRx_type F2R2;
			volatile CAN_FiRx_type F2R3;
			volatile CAN_FiRx_type F2R4;
			volatile CAN_FiRx_type F2R5;
			volatile CAN_FiRx_type F2R6;
			volatile CAN_FiRx_type F2R7;
			volatile CAN_FiRx_type F2R8;
			volatile CAN_FiRx_type F3R1;
			volatile CAN_FiRx_type F3R2;
			volatile CAN_FiRx_type F3R3;
			volatile CAN_FiRx_type F3R4;
			volatile CAN_FiRx_type F3R5;
			volatile CAN_FiRx_type F3R6;
			volatile CAN_FiRx_type F3R7;
			volatile CAN_FiRx_type F3R8;
		} ACCEPTANCE_FILTER_23;
		struct {
			volatile CAN_FiRx_type F4R1;
			volatile CAN_FiRx_type F4R2;
			volatile CAN_FiRx_type F4R3;
			volatile CAN_FiRx_type F4R4;
			volatile CAN_FiRx_type F4R5;
			volatile CAN_FiRx_type F4R6;
			volatile CAN_FiRx_type F4R7;
			volatile CAN_FiRx_type F4R8;
			volatile CAN_FiRx_type F5R1;
			volatile CAN_FiRx_type F5R2;
			volatile CAN_FiRx_type F5R3;
			volatile CAN_FiRx_type F5R4;
			volatile CAN_FiRx_type F5R5;
			volatile CAN_FiRx_type F5R6;
			volatile CAN_FiRx_type F5R7;
			volatile CAN_FiRx_type F5R8;
		} ACCEPTANCE_FILTER_45;
		CAN_MAILBOX_type TX_MAILBOX_2;
		CAN_CONFIGURATION_type CONFIGURATION_DIAGNOSTIC;
		CAN_MAILBOX_type RECEIVE_FIFO;
	};
} CAN_type;
#define CAN_BASE 0xFFFF
#define CAN ((CAN_type*)CAN_BASE)

// Block: Timers
typedef union {
	struct {
		uint8_t CEN:1; /*!< bit 0: Counter enable */
		uint8_t UDIS:1; /*!< bit 1: Update disable */
		uint8_t URS:1; /*!< bit 2: Update request source */
		uint8_t OPM:1; /*!< bit 3: One-pulse mode */
		uint8_t DIR:1; /*!< bit 4: Direction */
		uint8_t CMS:2; /*!< bit 5..6: Center-aligned mode selection */
		uint8_t ARPE:1; /*!< bit 7: Auto-reload preload enable */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} TIM1_CR1_type; /*!< Control register 1 (TIM1_CR1) */

typedef union {
	struct {
		uint8_t CEN:1; /*!< bit 0: Counter enable */
		uint8_t UDIS:1; /*!< bit 1: Update disable */
		uint8_t URS:1; /*!< bit 2: Update request source */
		uint8_t OPM:1; /*!< bit 3: One-pulse mode */
		uint8_t :3; /*!< bit 4..6: Reserved */
		uint8_t ARPE:1; /*!< bit 7: Auto-reload preload enable */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} TIM23456_CR1_type; /*!< Control register 1 (TIMx_CR1) */

typedef union {
	struct {
		uint8_t CCPC:1; /*!< bit 0: Capture/compare preloaded control */
		uint8_t :1; /*!< bit 1: Reserved */
		uint8_t COMS:1; /*!< bit 2: Capture/compare control update selection */
		uint8_t :1; /*!< bit 3: Reserved */
		uint8_t MMS:3; /*!< bit 4..6: Master mode selection */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} TIM1_CR2_type; /*!< Control register 2 (TIM1_CR2) */

typedef union {
	struct {
		uint8_t :4; /*!< bit 0..3: Reserved */
		uint8_t MMS:3; /*!< bit 4..6: Master mode selection */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} TIM56_CR2_type; /*!< Control register 2 (TIM5_CR2 and TIM6_CR2) */

typedef union {
	struct {
		uint8_t SMS:3; /*!< bit 0..2: Clock/trigger/slave mode selection */
		uint8_t :1; /*!< bit 3: Reserved */
		uint8_t TS:3; /*!< bit 4..6: Trigger selection */
		uint8_t MSM:1; /*!< bit 7: Master/slave mode */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} TIM156_SMCR_type; /*!< Slave mode control register (TIMx_SMCR) */

typedef union {
	struct {
		uint8_t ETF:4; /*!< bit 0..3: External trigger filter */
		uint8_t ETPS:2; /*!< bit 4..5: External trigger prescaler */
		uint8_t ECE:1; /*!< bit 6: External clock enable */
		uint8_t ETP:1; /*!< bit 7: External trigger polarity */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} TIM1_ETR_type; /*!< External trigger register (TIM1_ETR) */

typedef union {
	struct {
		uint8_t UIE:1; /*!< bit 0: Update interrupt enable */
		uint8_t CC1IE:1; /*!< bit 1: Capture/compare 1 interrupt enable */
		uint8_t CC2IE:1; /*!< bit 2: Capture/compare 2 interrupt enable */
		uint8_t CC3IE:1; /*!< bit 3: Capture/compare 3 interrupt enable */
		uint8_t CC4IE:1; /*!< bit 4: Capture/compare 4 interrupt enable */
		uint8_t CC5IE:1; /*!< bit 5: Capture/compare 5 interrupt enable */
		uint8_t TIE:1; /*!< bit 6: Trigger interrupt enable */
		uint8_t BIE:1; /*!< bit 7: Break interrupt enable */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} TIM1_IER_type; /*!< Interrupt enable register (TIM1_IER) */

typedef union {
	struct {
		uint8_t UIE:1; /*!< bit 0: Update interrupt enable */
		uint8_t CC1IE:1; /*!< bit 1: Capture/compare 1 interrupt enable */
		uint8_t CC2IE:1; /*!< bit 2: Capture/compare 2 interrupt enable */
		uint8_t CC3IE:1; /*!< bit 3: Capture/compare 3 interrupt enable */
		uint8_t :2; /*!< bit 4..5: Reserved */
		uint8_t TIE:1; /*!< bit 6: Trigger interrupt enable */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} TIM235_IER_type; /*!< Interrupt enable register (TIMx_IER) */

typedef union {
	struct {
		uint8_t UIE:1; /*!< bit 0: Update interrupt enable */
		uint8_t :5; /*!< bit 1..5: Reserved */
		uint8_t TIE:1; /*!< bit 6: Trigger interrupt enable */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} TIM46_IER_type; /*!< Interrupt enable register (TIM6_IER) */

typedef union {
	struct {
		uint8_t UIF:1; /*!< bit 0: Update interrupt flag */
		uint8_t CC1IF:1; /*!< bit 1: Capture/compare 1 interrupt flag */
		uint8_t CC2IF:1; /*!< bit 2: Capture/compare 2 interrupt flag */
		uint8_t CC3IF:1; /*!< bit 3: Capture/compare 3 interrupt flag */
		uint8_t CC4IF:1; /*!< bit 4: Capture/compare 4 interrupt flag */
		uint8_t CC5IF:1; /*!< bit 5: Capture/compare 5 interrupt flag */
		uint8_t TIF:1; /*!< bit 6: Trigger interrupt flag */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} TIM1_SR1_type; /*!< Status register 1 (TIM1_SR1) */

typedef union {
	struct {
		uint8_t UIF:1; /*!< bit 0: Update interrupt flag */
		uint8_t CC1IF:1; /*!< bit 1: Capture/compare 1 interrupt flag */
		uint8_t CC2IF:1; /*!< bit 2: Capture/compare 2 interrupt flag */
		uint8_t CC3IF:1; /*!< bit 3: Capture/compare 3 interrupt flag */
		uint8_t :2; /*!< bit 4..5: Reserved */
		uint8_t TIF:1; /*!< bit 6: Trigger interrupt flag */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} TIM235_SR1_type; /*!< Status register 1 (TIMx_SR1) */

typedef union {
	struct {
		uint8_t UIF:1; /*!< bit 0: Update interrupt flag */
		uint8_t :5; /*!< bit 1..5: Reserved */
		uint8_t TIF:1; /*!< bit 6: Trigger interrupt flag */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} TIM46_SR1_type; /*!< Status register 1 (TIM6_SR) */

typedef union {
	struct {
		uint8_t :1; /*!< bit 0: Reserved */
		uint8_t CC1OF:1; /*!< bit 1: Capture/compare 1 overcapture flag */
		uint8_t CC2OF:1; /*!< bit 2: Capture/compare 2 overcapture flag */
		uint8_t CC3OF:1; /*!< bit 3: Capture/compare 3 overcapture flag */
		uint8_t CC4OF:1; /*!< bit 4: Capture/compare 4 overcapture flag */
		uint8_t CC5OF:1; /*!< bit 5: Capture/compare 5 overcapture flag */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} TIM1_SR2_type; /*!< Status register 2 (TIM1_SR2) */

typedef union {
	struct {
		uint8_t :1; /*!< bit 0: Reserved */
		uint8_t CC1OF:1; /*!< bit 1: Capture/compare 1 overcapture flag */
		uint8_t CC2OF:1; /*!< bit 2: Capture/compare 2 overcapture flag */
		uint8_t CC3OF:1; /*!< bit 3: Capture/compare 3 overcapture flag */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} TIM235_SR2_type; /*!< Status register 2 (TIMx_SR2) */

typedef union {
	struct {
		uint8_t UG:1; /*!< bit 0: Update generation */
		uint8_t CC1G:1; /*!< bit 1: Capture/compare 1 generation */
		uint8_t CC2G:1; /*!< bit 2: Capture/compare 2 generation */
		uint8_t CC3G:1; /*!< bit 3: Capture/compare 3 generation */
		uint8_t CC4G:1; /*!< bit 4: Capture/compare 4 generation */
		uint8_t CC5G:1; /*!< bit 5: Capture/compare 5 generation */
		uint8_t TG:1; /*!< bit 6: Trigger generation */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} TIM1_EGR_type; /*!< Event generation register (TIM1_EGR) */

typedef union {
	struct {
		uint8_t UG:1; /*!< bit 0: Update generation */
		uint8_t CC1G:1; /*!< bit 1: Capture/compare 1 generation */
		uint8_t CC2G:1; /*!< bit 2: Capture/compare 2 generation */
		uint8_t CC3G:1; /*!< bit 3: Capture/compare 3 generation */
		uint8_t :2; /*!< bit 4..5: Reserved */
		uint8_t TG:1; /*!< bit 6: Trigger generation */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} TIM235_EGR_type; /*!< Event generation register (TIMx_EGR) */

typedef union {
	struct {
		uint8_t UG:1; /*!< bit 0: Update generation */
		uint8_t :5; /*!< bit 1..5: Reserved */
		uint8_t TG:1; /*!< bit 6: Trigger generation */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} TIM46_EGR_type; /*!< Event generation register (TIMx_EGR) */

typedef union {
	struct {
		uint8_t CC1S:2; /*!< bit 0..1: Capture/compare 1 selection */
		uint8_t OC1FE:1; /*!< bit 2: Output compare 1 fast enable */
		uint8_t OC1PE:1; /*!< bit 3: Output compare 1 preload enable */
		uint8_t OC1M:3; /*!< bit 4..6: Output compare 1 mode */
		uint8_t OC1CE:1; /*!< bit 7: Output compare 1 clear enable */
	} output_fields; /*!< structure used for bit access */
	struct {
		uint8_t CC1S:2; /*!< bit 0..1: Capture/compare 1 selection */
		uint8_t IC1PSC:2; /*!< bit 2..3: Input capture 1 prescaler */
		uint8_t IC1F:4; /*!< bit 4..7: Input capture 1 filter */
	} input_fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} TIM1_CCMR1_type; /*!< Capture/compare mode register 1 (TIM1_CCMR1) */

typedef union {
	struct {
		uint8_t CC1S:2; /*!< bit 0..1: Capture/compare 1 selection */
		uint8_t :1; /*!< bit 2: Reserved */
		uint8_t OC1PE:1; /*!< bit 3: Output compare 1 preload enable */
		uint8_t OC1M:3; /*!< bit 4..6: Output compare 1 mode */
	} output_fields; /*!< structure used for bit access */
	struct {
		uint8_t CC1S:2; /*!< bit 0..1: Capture/compare 1 selection */
		uint8_t IC1PSC:2; /*!< bit 2..3: Input capture 1 prescaler */
		uint8_t IC1F:4; /*!< bit 4..7: Input capture 1 filter */
	} input_fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} TIM235_CCMR1_type; /*!< Capture/compare mode register 1 (TIMx_CCMR1) */

typedef union {
	struct {
		uint8_t CC2S:2; /*!< bit 0..1: Capture/compare 2 selection */
		uint8_t OC2FE:1; /*!< bit 2: Output compare 2 fast enable */
		uint8_t OC2PE:1; /*!< bit 3: Output compare 2 preload enable */
		uint8_t OC2M:3; /*!< bit 4..6: Output compare 2 mode */
		uint8_t OC2CE:1; /*!< bit 7: Output compare 2 clear enable */
	} output_fields; /*!< structure used for bit access */
	struct {
		uint8_t CC2S:2; /*!< bit 0..1: Capture/compare 2 selection */
		uint8_t IC2PCS:2; /*!< bit 2..3: Input capture 2 prescaler */
		uint8_t IC2F:4; /*!< bit 4..8: Input capture 2 filter */
	} input_fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} TIM1_CCMR2_type; /*!< Capture/compare mode register 2 (TIM1_CCMR2) */

typedef union {
	struct {
		uint8_t CC2S:2; /*!< bit 0..1: Capture/compare 2 selection */
		uint8_t :1; /*!< bit 2: Reserved */
		uint8_t OC2PE:1; /*!< bit 3: Output compare 2 preload enable */
		uint8_t OC2M:3; /*!< bit 4..6: Output compare 2 mode */
	} output_fields; /*!< structure used for bit access */
	struct {
		uint8_t CC2S:2; /*!< bit 0..1: Capture/compare 2 selection */
		uint8_t IC2PCS:2; /*!< bit 2..3: Input capture 2 prescaler */
		uint8_t IC2F:4; /*!< bit 4..8: Input capture 2 filter */
	} input_fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} TIM235_CCMR2_type; /*!< Capture/compare mode register 2 (TIMx_CCMR2) */

typedef union {
	struct {
		uint8_t CC3S:2; /*!< bit 0..1: Capture/compare 3 selection */
		uint8_t OC3FE:1; /*!< bit 2: Output compare 3 fast enable */
		uint8_t OC3PE:1; /*!< bit 3: Output compare 3 preload enable */
		uint8_t OC3M:3; /*!< bit 4..6: Output compare 3 mode */
		uint8_t OC3CE:1; /*!< bit 7: Output compare 3 clear enable */
	} output_fields; /*!< structure used for bit access */
	struct {
		uint8_t CC3S:2; /*!< bit 0..1: Capture/compare 3 selection */
		uint8_t IC3PSC:2; /*!< bit 2..3: Input capture 3 prescaler */
		uint8_t IC3F:4; /*!< bit 4..7: Input capture 3 filter */
	} input_fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} TIM1_CCMR3_type; /*!< Capture/compare mode register 3 (TIM1_CCMR3) */

typedef union {
	struct {
		uint8_t CC3S:2; /*!< bit 0..1: Capture/compare 3 selection */
		uint8_t :1; /*!< bit 2: Reserved */
		uint8_t OC3PE:1; /*!< bit 3: Output compare 3 preload enable */
		uint8_t OC3M:3; /*!< bit 4..6: Output compare 3 mode */
	} output_fields; /*!< structure used for bit access */
	struct {
		uint8_t CC3S:2; /*!< bit 0..1: Capture/compare 3 selection */
		uint8_t IC3PSC:2; /*!< bit 2..3: Input capture 3 prescaler */
		uint8_t IC3F:4; /*!< bit 4..7: Input capture 3 filter */
	} input_fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} TIM235_CCMR3_type; /*!< Capture/compare mode register 3 (TIMx_CCMR3) */

typedef union {
	struct {
		uint8_t CC4S:2; /*!< bit 0..1: Capture/compare 4 selection */
		uint8_t OC4FE:1; /*!< bit 2: Output compare 4 fast enable */
		uint8_t OC4PE:1; /*!< bit 3: Output compare 4 preload enable */
		uint8_t OC4M:3; /*!< bit 4..6: Output compare 4 mode */
		uint8_t OC4CE:1; /*!< bit 7: Output compare 4 clear enable */
	} output_fields; /*!< structure used for bit access */
	struct {
		uint8_t CC4S:2; /*!< bit 0..1: Capture/compare 4 selection */
		uint8_t IC4PSC:2; /*!< bit 2..3: Input capture 4 prescaler */
		uint8_t IC4F:4; /*!< bit 4..7: Input capture 4 filter */
	} input_fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} TIM1_CCMR4_type; /*!< Capture/compare mode register 4 (TIM1_CCMR4) */

typedef union {
	struct {
		uint8_t CC1E:1; /*!< bit 0: Capture/Compare 1 output Enable */
		uint8_t CC1P:1; /*!< bit 1: Capture/compare 1 output polarity */
		uint8_t CC1NE:1; /*!< bit 2: Capture/compare 1 complementary output enable */
		uint8_t CC1NP:1; /*!< bit 3: Capture/compare 1 complementary output polarity */
		uint8_t CC2E:1; /*!< bit 4: Capture/compare 2 output enable */
		uint8_t CC2P:1; /*!< bit 5: Capture/compare 2 output polarity */
		uint8_t CC2NE:1; /*!< bit 6: Capture/compare 2 complementary output enable */
		uint8_t CC2NP:1; /*!< bit 7: Capture/compare 2 complementary output polarity */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} TIM1_CCER1_type; /*!< Capture/compare enable register 1 (TIM1_CCER1) */

typedef union {
	struct {
		uint8_t CC1E:1; /*!< bit 0: Capture/Compare 1 output Enable */
		uint8_t CC1P:1; /*!< bit 1: Capture/compare 1 output polarity */
		uint8_t :2; /*!< bit 2..3: Reserved */
		uint8_t CC2E:1; /*!< bit 4: Capture/compare 2 output enable */
		uint8_t CC2P:1; /*!< bit 5: Capture/compare 2 output polarity */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} TIM235_CCER1_type; /*!< Capture/compare enable register 1 (TIMx_CCER1) */

typedef union {
	struct {
		uint8_t CC3E:1; /*!< bit 0: Capture/compare 3 output enable */
		uint8_t CC3P:1; /*!< bit 1: Capture/compare 3 output polarity */
		uint8_t CC3NE:1; /*!< bit 2: Capture/compare 3 complementary output enable */
		uint8_t CC3NP:1; /*!< bit 3: Capture/compare 3 complementary output polarity */
		uint8_t CC4E:1; /*!< bit 4: Capture/compare 4 output enable */
		uint8_t CC4P:1; /*!< bit 5: Capture/compare 4 output polarity */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} TIM1_CCER2_type; /*!< Capture/compare enable register 2 (TIM1_CCER2) */

typedef union {
	struct {
		uint8_t CC3E:1; /*!< bit 0: Capture/compare 3 output enable */
		uint8_t CC3P:1; /*!< bit 1: Capture/compare 3 output polarity */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} TIM235_CCER2_type; /*!< Capture/compare enable register 2 (TIMx_CCER2) */

typedef union {
	struct {
		uint8_t CNT15_8:8; /*!< bit 0..7: Counter value (MSB) */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} TIM1235_CNTRH_type; /*!< Counter high (TIMx_CNTRH) */

typedef union {
	struct {
		uint8_t CNT7_0:8; /*!< bit 0..7: Counter value (LSB) */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} TIM1235_CNTRL_type; /*!< Counter low (TIMx_CNTRL) */

typedef union {
	struct {
		uint8_t CNT:8; /*!< bit 0..7: Counter value */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} TIM46_CNTR_type; /*!< Counter value (TIMx_CNTR) */

typedef union {
	struct {
		uint8_t PSC15_8:8; /*!< bit 0..7: Prescaler value (MSB) */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} TIM1_PSCRH_type; /*!< Prescaler high (TIM1_PSCRH) */

typedef union {
	struct {
		uint8_t PSC7_0:8; /*!< bit 0..7: Prescaler value (LSB) */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} TIM1_PSCRL_type; /*!< Prescaler low (TIM1_PSCRL) */

typedef union {
	struct {
		uint8_t PSC:4; /*!< bit 0..3: Prescaler value */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} TIM235_PSCR_type; /*!< Prescaler register (TIMx_PSCR) */

typedef union {
	struct {
		uint8_t PSC:3; /*!< bit 0..2: Prescaler value */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} TIM46_PSCR_type; /*!< Prescaler register (TIMx_PSCR) */

typedef union {
	struct {
		uint8_t ARR15_8:8; /*!< bit 0..7: Auto-reload value (MSB) */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} TIM1235_ARRH_type; /*!< Auto-reload register high (TIMx_ARRH) */

typedef union {
	struct {
		uint8_t ARR7_0:8; /*!< bit 0..7: Auto-reload value (LSB) */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} TIM1235_ARRL_type; /*!< Auto-reload register low (TIMx_ARRL) */

typedef union {
	struct {
		uint8_t ARR:8; /*!< bit 0..7: Auto-reload value (LSB) */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} TIM46_ARR_type; /*!< Auto-reload register (TIMx_ARR) */

typedef union {
	struct {
		uint8_t REP:8; /*!< bit 0..7: Repetition counter value */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} TIM1_RCR_type; /*!< Repetition counter register (TIM1_RCR) */

typedef union {
	struct {
		uint8_t CCR1:8; /*!< bit 0..7: Capture/compare 1 value (MSB) */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} TIM1235_CCR1H_type; /*!< Capture/compare register 1 high (TIMx_CCR1H) */

typedef union {
	struct {
		uint8_t CCR1:8; /*!< bit 0..7: Capture/compare 1 value (LSB) */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} TIM1235_CCR1L_type; /*!< Capture/compare register 1 low (TIMx_CCR1L) */

typedef union {
	struct {
		uint8_t CCR2:8; /*!< bit 0..7: Capture/compare 2 value (MSB) */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} TIM1235_CCR2H_type; /*!< Capture/compare register 2 high (TIMx_CCR2H) */

typedef union {
	struct {
		uint8_t CCR2:8; /*!< bit 0..7: Capture/compare 2 value (LSB) */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} TIM1235_CCR2L_type; /*!< Capture/compare register 2 low (TIMx_CCR2L) */

typedef union {
	struct {
		uint8_t CCR3:8; /*!< bit 0..7: Capture/compare 3 value (MSB) */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} TIM1235_CCR3H_type; /*!< Capture/compare register 3 high (TIMx_CCR3H) */

typedef union {
	struct {
		uint8_t CCR3:8; /*!< bit 0..7: Capture/compare 3 value (LSB) */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} TIM1235_CCR3L_type; /*!< Capture/compare register 3 low (TIMx_CCR3L) */

typedef union {
	struct {
		uint8_t CCR4:8; /*!< bit 0..7: Capture/compare 4 value (MSB) */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} TIM1_CCR4H_type; /*!< Capture/compare register 4 high (TIM1_CCR4H) */

typedef union {
	struct {
		uint8_t CCR4:8; /*!< bit 0..7: Capture/compare 4 value (LSB) */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} TIM1_CCR4L_type; /*!< Capture/compare register 4 low (TIMx_CCR4L) */

typedef union {
	struct {
		uint8_t LOCK:2; /*!< bit 0..1: Lock configuration */
		uint8_t OSSI:1; /*!< bit 2: Off state selection for idle mode */
		uint8_t OSSR:1; /*!< bit 3: Off state selection for Run mode */
		uint8_t BKE:1; /*!< bit 4: Break enable */
		uint8_t BKP:1; /*!< bit 5: Break polarity */
		uint8_t AOE:1; /*!< bit 6: Automatic output enable */
		uint8_t MOE:1; /*!< bit 7: Main output enable */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} TIM1_BKR_type; /*!< Break register (TIM1_BKR) */

typedef union {
	struct {
		uint8_t DTG:8; /*!< bit 0..7: Deadtime generator set-up */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} TIM1_DTR_type; /*!< Deadtime register (TIM1_DTR) */

typedef union {
	struct {
		uint8_t OIS1:1; /*!< bit 0: Output idle state 1 (OC1 output) */
		uint8_t OIS1N:1; /*!< bit 1: Output idle state 1 (OC1N output) */
		uint8_t OIS2:1; /*!< bit 2: Output idle state 2 (OC2 output) */
		uint8_t OIS2N:1; /*!< bit 3: Output idle state 2 (OC2N output) */
		uint8_t OIS3:1; /*!< bit 4: Output idle state 3 (OC3 output) */
		uint8_t OIS3N:1; /*!< bit 5: Output idle state 3 (OC3N output) */
		uint8_t OIS4:1; /*!< bit 6: Output idle state 4 (OC4 output) */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} TIM1_OISR_type; /*!< Output idle state register (TIM1_OISR) */

typedef struct {
	volatile TIM1_CR1_type CR1;
	volatile TIM1_CR1_type CR2;
	volatile TIM156_SMCR_type SMCR;
	volatile TIM1_ETR_type ETR;
	volatile TIM1_IER_type IER;
	volatile TIM1_SR1_type SR1;
	volatile TIM1_SR2_type SR2;
	volatile TIM1_EGR_type EGR;
	volatile TIM1_CCMR1_type CCMR1;
	volatile TIM1_CCMR2_type CCMR2;
	volatile TIM1_CCMR3_type CCMR3;
	volatile TIM1_CCMR4_type CCMR4;
	volatile TIM1_CCER1_type CCER1;
	volatile TIM1_CCER2_type CCER2;
	volatile TIM1235_CNTRH_type CNTRH;
	volatile TIM1235_CNTRL_type CNTRL;
	volatile TIM1_PSCRH_type PSCRH;
	volatile TIM1_PSCRL_type PSCRL;
	volatile TIM1235_ARRH_type ARRH;
	volatile TIM1235_ARRL_type ARRL;
	volatile TIM1_RCR_type RCR;
	volatile TIM1235_CCR1H_type CCR1H;
	volatile TIM1235_CCR1L_type CCR1L;
	volatile TIM1235_CCR2H_type CCR2H;
	volatile TIM1235_CCR2L_type CCR2L;
	volatile TIM1235_CCR3H_type CCR3H;
	volatile TIM1235_CCR3L_type CCR3L;
	volatile TIM1_CCR4H_type CCR4H;
	volatile TIM1_CCR4L_type CCR4L;
	volatile TIM1_BKR_type BKR;
	volatile TIM1_DTR_type DTR;
	volatile TIM1_OISR_type OISR;
} TIM1_type;
#define TIM1_BASE 0x5250
#define TIM1 ((TIM1_type*)TIM1_BASE)

typedef struct {
	volatile TIM23456_CR1_type CR1;
	const uint8_t reserved[2]; // PRODUCT this is product dependent
	volatile TIM235_IER_type IER;
	volatile TIM235_SR1_type SR1;
	volatile TIM235_SR2_type SR2;
	volatile TIM235_EGR_type EGR;
	volatile TIM235_CCMR1_type CCMR1;
	volatile TIM235_CCMR2_type CCMR2;
	volatile TIM235_CCMR3_type CCMR3;
	volatile TIM235_CCER1_type CCER1;
	volatile TIM235_CCER2_type CCER2;
	volatile TIM1235_CNTRH_type CNTRH;
	volatile TIM1235_CNTRL_type CNTRL;
	volatile TIM235_PSCR_type PSCR;
	volatile TIM1235_ARRH_type ARRH;
	volatile TIM1235_ARRL_type ARRL;
	volatile TIM1235_CCR1H_type CCR1H;
	volatile TIM1235_CCR1L_type CCR1L;
	volatile TIM1235_CCR2H_type CCR2H;
	volatile TIM1235_CCR2L_type CCR2L;
	volatile TIM1235_CCR3H_type CCR3H;
	volatile TIM1235_CCR3L_type CCR3L;
} TIM2_type;
#define TIM2_BASE 0x5300
#define TIM2 ((TIM2_type*)TIM2_BASE)

typedef struct {
	volatile TIM23456_CR1_type CR1;
	volatile TIM235_IER_type IER;
	volatile TIM235_SR1_type SR1;
	volatile TIM235_SR2_type SR2;
	volatile TIM235_EGR_type EGR;
	volatile TIM235_CCMR1_type CCMR1;
	volatile TIM235_CCMR2_type CCMR2;
	volatile TIM235_CCER1_type CCER1;
	volatile TIM1235_CNTRH_type CNTRH;
	volatile TIM1235_CNTRL_type CNTRL;
	volatile TIM235_PSCR_type PSCR;
	volatile TIM1235_ARRH_type ARRH;
	volatile TIM1235_ARRL_type ARRL;
	volatile TIM1235_CCR1H_type CCR1H;
	volatile TIM1235_CCR1L_type CCR1L;
	volatile TIM1235_CCR2H_type CCR2H;
	volatile TIM1235_CCR2L_type CCR2L;
} TIM3_type;
#define TIM3_BASE 0xFFFF
#define TIM3 ((TIM6_type*)TIM3_BASE)

typedef struct {
	volatile TIM23456_CR1_type CR1;
	const uint8_t reserved[2]; // PRODUCT this is product dependent
	volatile TIM46_IER_type IER;
	volatile TIM46_SR1_type SR;
	volatile TIM46_EGR_type EGR;
	volatile TIM46_CNTR_type CNTR;
	volatile TIM46_PSCR_type PSCR;
	volatile TIM46_ARR_type ARR;
} TIM4_type;
#define TIM4_BASE 0x5340
#define TIM4 ((TIM4_type*)TIM4_BASE)

typedef struct {
	volatile TIM23456_CR1_type CR1;
	volatile TIM56_CR2_type CR2;
	volatile TIM156_SMCR_type SMCR;
	volatile TIM235_IER_type IER;
	volatile TIM235_SR1_type SR1;
	volatile TIM235_SR2_type SR2;
	volatile TIM235_EGR_type EGR;
	volatile TIM235_CCMR1_type CCMR1;
	volatile TIM235_CCMR2_type CCMR2;
	volatile TIM235_CCMR3_type CCMR3;
	volatile TIM235_CCER1_type CCER1;
	volatile TIM235_CCER1_type CCER2;
	volatile TIM1235_CNTRH_type CNTRH;
	volatile TIM1235_CNTRL_type CNTRL;
	volatile TIM235_PSCR_type PSCR;
	volatile TIM1235_ARRH_type ARRH;
	volatile TIM1235_ARRL_type ARRL;
	volatile TIM1235_CCR1H_type CCR1H;
	volatile TIM1235_CCR1L_type CCR1L;
	volatile TIM1235_CCR2H_type CCR2H;
	volatile TIM1235_CCR2L_type CCR2L;
	volatile TIM1235_CCR2H_type CCR3H;
	volatile TIM1235_CCR2L_type CCR3L;
} TIM5_type;
#define TIM5_BASE 0xFFFF
#define TIM5 ((TIM5_type*)TIM5_BASE)

typedef struct {
	volatile TIM23456_CR1_type CR1;
	volatile TIM56_CR2_type CR2;
	volatile TIM156_SMCR_type SMCR;
	volatile TIM46_IER_type IER;
	volatile TIM46_SR1_type SR1;
	volatile TIM46_EGR_type EGR;
	volatile TIM46_CNTR_type CNTR;
	volatile TIM46_PSCR_type PSCR;
	volatile TIM46_ARR_type ARR;
} TIM6_type;
#define TIM6_BASE 0xFFFF
#define TIM6 ((TIM6_type*)TIM6_BASE)

// Block: TIM1
#define TIM1_BASE 0x5250
#define TIM1_CR1 (*(volatile uint8_t *)(TIM1_BASE + 0x00))
#define TIM1_CR2 (*(volatile uint8_t *)(TIM1_BASE + 0x01))
#define TIM1_SMCR (*(volatile uint8_t *)(TIM1_BASE + 0x02))
#define TIM1_ETR (*(volatile uint8_t *)(TIM1_BASE + 0x03))
#define TIM1_IER (*(volatile uint8_t *)(TIM1_BASE + 0x04))
#define TIM1_SR1 (*(volatile uint8_t *)(TIM1_BASE + 0x05))
#define TIM1_SR2 (*(volatile uint8_t *)(TIM1_BASE + 0x06))
#define TIM1_EGR (*(volatile uint8_t *)(TIM1_BASE + 0x07))
#define TIM1_CCMR1 (*(volatile uint8_t *)(TIM1_BASE + 0x08))
#define TIM1_CCMR2 (*(volatile uint8_t *)(TIM1_BASE + 0x09))
#define TIM1_CCMR3 (*(volatile uint8_t *)(TIM1_BASE + 0x0A))
#define TIM1_CCMR4 (*(volatile uint8_t *)(TIM1_BASE + 0x0B))
#define TIM1_CCER1 (*(volatile uint8_t *)(TIM1_BASE + 0x0C))
#define TIM1_CCER2 (*(volatile uint8_t *)(TIM1_BASE + 0x0D))
#define TIM1_CNTRH (*(volatile uint8_t *)(TIM1_BASE + 0x0E))
#define TIM1_CNTRL (*(volatile uint8_t *)(TIM1_BASE + 0x0F))
#define TIM1_PSCRH (*(volatile uint8_t *)(TIM1_BASE + 0x10))
#define TIM1_PSCRL (*(volatile uint8_t *)(TIM1_BASE + 0x11))
#define TIM1_ARRH (*(volatile uint8_t *)(TIM1_BASE + 0x12))
#define TIM1_ARRL (*(volatile uint8_t *)(TIM1_BASE + 0x13))
#define TIM1_RCR (*(volatile uint8_t *)(TIM1_BASE + 0x14))
#define TIM1_CCR1H (*(volatile uint8_t *)(TIM1_BASE + 0x15))
#define TIM1_CCR1L (*(volatile uint8_t *)(TIM1_BASE + 0x16))
#define TIM1_CCR2H (*(volatile uint8_t *)(TIM1_BASE + 0x17))
#define TIM1_CCR2L (*(volatile uint8_t *)(TIM1_BASE + 0x18))
#define TIM1_CCR3H (*(volatile uint8_t *)(TIM1_BASE + 0x19))
#define TIM1_CCR3L (*(volatile uint8_t *)(TIM1_BASE + 0x1A))
#define TIM1_CCR4H (*(volatile uint8_t *)(TIM1_BASE + 0x1B))
#define TIM1_CCR4L (*(volatile uint8_t *)(TIM1_BASE + 0x1C))
#define TIM1_BKR (*(volatile uint8_t *)(TIM1_BASE + 0x1D))
#define TIM1_DTR (*(volatile uint8_t *)(TIM1_BASE + 0x1E))
#define TIM1_OISR (*(volatile uint8_t *)(TIM1_BASE + 0x1F))

// Block: TIM2
#define TIM2_CR1 (*(volatile uint8_t *)(TIM2_BASE + 0x00))
#define TIM2_CR1_CEN (1U << 0)
#define TIM2_CR1_UDIS (1U << 1)
#define TIM2_CR1_URS (1U << 2)
#define TIM2_CR1_OPM (1U << 3)
#define TIM2_CR1_APRE (1U << 7)
#define TIM2_IER (*(volatile uint8_t *)(TIM2_BASE + 0x03))
#define TIM2_IER_UIE (1U << 0)
#define TIM2_IER_CC1IE (1U << 1)
#define TIM2_IER_CC2IE (1U << 2)
#define TIM2_IER_CC3IE (1U << 3)
#define TIM2_IER_TIE (1U << 6)
#define TIM2_SR1 (*(volatile uint8_t *)(TIM2_BASE + 0x04))
#define TIM2_SR1_UIF (1U << 0)
#define TIM2_SR1_CC1IF (1U << 1)
#define TIM2_SR1_CC2IF (1U << 2)
#define TIM2_SR1_CC3IF (1U << 3)
#define TIM2_SR1_TIF (1U << 6)
#define TIM2_SR2 (*(volatile uint8_t *)(TIM2_BASE + 0x05))
#define TIM2_SR2_CC1OF (1U << 1)
#define TIM2_SR2_CC2OF (1U << 2)
#define TIM2_SR2_CC3OF (1U << 3)
#define TIM2_EGR (*(volatile uint8_t *)(TIM2_BASE + 0x06))
#define TIM2_EGR_CC1G (1U << 1)
#define TIM2_EGR_CC2G (1U << 2)
#define TIM2_EGR_CC3G (1U << 3)
#define TIM2_EGR_TG (1U << 6)
#define TIM2_CCMR1 (*(volatile uint8_t *)(TIM2_BASE + 0x07))
#define TIM2_CCMR1_CC1S_OFFSET 0
#define TIM2_CCMR1_CC1S_MASK 0x3
#define TIM2_CCMR1_CC1PE (1U << 3)
#define TIM2_CCMR1_OC1M_OFFSET 4
#define TIM2_CCMR1_OC1M_MASK 0x7
#define TIM2_CCMR1_CC1S_OFFSET 0
#define TIM2_CCMR1_CC1S_MASK 0x3
#define TIM2_CCMR1_IC1PSC_OFFSET 2
#define TIM2_CCMR1_IC1PSC_MASK 0x3
#define TIM2_CCMR1_IC1F_OFFSET 4
#define TIM2_CCMR1_IC1F_MASK 0xf
#define TIM2_CCMR2 (*(volatile uint8_t *)(TIM2_BASE + 0x08))
#define TIM2_CCMR2_CC2S_OFFSET 0
#define TIM2_CCMR2_CC2S_MASK 0x3
#define TIM2_CCMR2_CC2PE (1U << 3)
#define TIM2_CCMR2_OC2M_OFFSET 4
#define TIM2_CCMR2_OC2M_MASK 0x7
#define TIM2_CCMR2_CC2S_OFFSET 0
#define TIM2_CCMR2_CC2S_MASK 0x3
#define TIM2_CCMR2_IC2PSC_OFFSET 2
#define TIM2_CCMR2_IC2PSC_MASK 0x3
#define TIM2_CCMR2_IC2F_OFFSET 4
#define TIM2_CCMR2_IC2F_MASK 0xf
#define TIM2_CCMR3 (*(volatile uint8_t *)(TIM2_BASE + 0x09))
#define TIM2_CCMR3_CC3S_OFFSET 0
#define TIM2_CCMR3_CC3S_MASK 0x3
#define TIM2_CCMR3_CC3PE (1U << 3)
#define TIM2_CCMR3_OC3M_OFFSET 4
#define TIM2_CCMR3_OC3M_MASK 0x7
#define TIM2_CCMR3_CC3S_OFFSET 0
#define TIM2_CCMR3_CC3S_MASK 0x3
#define TIM2_CCMR3_IC3PSC_OFFSET 2
#define TIM2_CCMR3_IC3PSC_MASK 0x3
#define TIM2_CCMR3_IC3F_OFFSET 4
#define TIM2_CCMR3_IC3F_MASK 0xf
#define TIM2_CCER1 (*(volatile uint8_t *)(TIM2_BASE + 0x0A))
#define TIM2_CCER1_CC1E (1U << 0)
#define TIM2_CCER1_CC1P (1U << 1)
#define TIM2_CCER1_CC2E (1U << 4)
#define TIM2_CCER1_CC2P (1U << 5)
#define TIM2_CCER2 (*(volatile uint8_t *)(TIM2_BASE + 0x0B))
#define TIM2_CCER2_CC3E (1U << 0)
#define TIM2_CCER2_CC3P (1U << 1)
#define TIM2_CNTRH (*(volatile uint8_t *)(TIM2_BASE + 0x0C))
#define TIM2_CNTRL (*(volatile uint8_t *)(TIM2_BASE + 0x0D))
#define TIM2_PSCR (*(volatile uint8_t *)(TIM2_BASE + 0x0E))
#define TIM2_PSCR_OFFSET 0
#define TIM2_PSCR_MASK 0xf
#define TIM2_ARRH (*(volatile uint8_t *)(TIM2_BASE + 0x0F))
#define TIM2_ARRL (*(volatile uint8_t *)(TIM2_BASE + 0x10))
#define TIM2_CCR1H (*(volatile uint8_t *)(TIM2_BASE + 0x11))
#define TIM2_CCR1L (*(volatile uint8_t *)(TIM2_BASE + 0x12))
#define TIM2_CCR2H (*(volatile uint8_t *)(TIM2_BASE + 0x13))
#define TIM2_CCR2L (*(volatile uint8_t *)(TIM2_BASE + 0x14))
#define TIM2_CCR3H (*(volatile uint8_t *)(TIM2_BASE + 0x15))
#define TIM2_CCR3L (*(volatile uint8_t *)(TIM2_BASE + 0x16))

// Block: TIM3
#define TIM3_CR1 (*(volatile uint8_t *)(TIM3_BASE + 0x00))
#define TIM3_CR1_CEN (1U << 0)
#define TIM3_CR1_UDIS (1U << 1)
#define TIM3_CR1_URS (1U << 2)
#define TIM3_CR1_OPM (1U << 3)
#define TIM3_CR1_APRE (1U << 7)
#define TIM3_IER (*(volatile uint8_t *)(TIM3_BASE + 0x01))
#define TIM3_IER_UIE (1U << 0)
#define TIM3_IER_CC1IE (1U << 1)
#define TIM3_IER_CC2IE (1U << 2)
#define TIM3_IER_CC3IE (1U << 3)
#define TIM3_IER_TIE (1U << 6)
#define TIM3_SR1 (*(volatile uint8_t *)(TIM3_BASE + 0x02))
#define TIM3_SR1_UIF (1U << 0)
#define TIM3_SR1_CC1IF (1U << 1)
#define TIM3_SR1_CC2IF (1U << 2)
#define TIM3_SR1_CC3IF (1U << 3)
#define TIM3_SR1_TIF (1U << 6)
#define TIM3_SR2 (*(volatile uint8_t *)(TIM3_BASE + 0x03))
#define TIM3_SR2_CC1OF (1U << 1)
#define TIM3_SR2_CC2OF (1U << 2)
#define TIM3_SR2_CC3OF (1U << 3)
#define TIM3_EGR (*(volatile uint8_t *)(TIM3_BASE + 0x04))
#define TIM3_EGR_CC1G (1U << 1)
#define TIM3_EGR_CC2G (1U << 2)
#define TIM3_EGR_CC3G (1U << 3)
#define TIM3_EGR_TG (1U << 6)
#define TIM3_CCMR1 (*(volatile uint8_t *)(TIM3_BASE + 0x05))
#define TIM3_CCMR1_CC1S_OFFSET 0
#define TIM3_CCMR1_CC1S_MASK 0x3
#define TIM3_CCMR1_CC1PE (1U << 3)
#define TIM3_CCMR1_OC1M_OFFSET 4
#define TIM3_CCMR1_OC1M_MASK 0x7
#define TIM3_CCMR1_CC1S_OFFSET 0
#define TIM3_CCMR1_CC1S_MASK 0x3
#define TIM3_CCMR1_IC1PSC_OFFSET 2
#define TIM3_CCMR1_IC1PSC_MASK 0x3
#define TIM3_CCMR1_IC1F_OFFSET 4
#define TIM3_CCMR1_IC1F_MASK 0xf
#define TIM3_CCMR2 (*(volatile uint8_t *)(TIM3_BASE + 0x06))
#define TIM3_CCMR2_CC2S_OFFSET 0
#define TIM3_CCMR2_CC2S_MASK 0x3
#define TIM3_CCMR2_CC2PE (1U << 3)
#define TIM3_CCMR2_OC2M_OFFSET 4
#define TIM3_CCMR2_OC2M_MASK 0x7
#define TIM3_CCMR2_CC2S_OFFSET 0
#define TIM3_CCMR2_CC2S_MASK 0x3
#define TIM3_CCMR2_IC2PSC_OFFSET 2
#define TIM3_CCMR2_IC2PSC_MASK 0x3
#define TIM3_CCMR2_IC2F_OFFSET 4
#define TIM3_CCMR2_IC2F_MASK 0xf
#define TIM3_CCER1 (*(volatile uint8_t *)(TIM3_BASE + 0x07))
#define TIM3_CCER1_CC1E (1U << 0)
#define TIM3_CCER1_CC1P (1U << 1)
#define TIM3_CCER1_CC2E (1U << 4)
#define TIM3_CCER1_CC2P (1U << 5)
#define TIM3_CNTRH (*(volatile uint8_t *)(TIM3_BASE + 0x08))
#define TIM3_CNTRL (*(volatile uint8_t *)(TIM3_BASE + 0x09))
#define TIM3_PSCR (*(volatile uint8_t *)(TIM3_BASE + 0x0A))
#define TIM3_PSCR_OFFSET 0
#define TIM3_PSCR_MASK 0xf
#define TIM3_ARRH (*(volatile uint8_t *)(TIM3_BASE + 0x0B))
#define TIM3_ARRL (*(volatile uint8_t *)(TIM3_BASE + 0x0C))
#define TIM3_CCR1H (*(volatile uint8_t *)(TIM3_BASE + 0x0D))
#define TIM3_CCR1L (*(volatile uint8_t *)(TIM3_BASE + 0x0E))
#define TIM3_CCR2H (*(volatile uint8_t *)(TIM3_BASE + 0x0F))
#define TIM3_CCR2L (*(volatile uint8_t *)(TIM3_BASE + 0x10))

// Block: TIM4
#define TIM4_CR1 (*(volatile uint8_t *)(TIM4_BASE + 0x00))
#define TIM4_IER (*(volatile uint8_t *)(TIM4_BASE + 0x03))
#define TIM4_SR (*(volatile uint8_t *)(TIM4_BASE + 0x04))
#define TIM4_EGR (*(volatile uint8_t *)(TIM4_BASE + 0x05))
#define TIM4_CNTR (*(volatile uint8_t *)(TIM4_BASE + 0x06))
#define TIM4_PSCR (*(volatile uint8_t *)(TIM4_BASE + 0x07))
#define TIM4_ARR (*(volatile uint8_t *)(TIM4_BASE + 0x08))

// Block: TIM5
#define TIM5_CR1 (*(volatile uint8_t *)(TIM5_BASE + 0x00))
#define TIM5_CR1_CEN (1U << 0)
#define TIM5_CR1_UDIS (1U << 1)
#define TIM5_CR1_URS (1U << 2)
#define TIM5_CR1_OPM (1U << 3)
#define TIM5_CR1_APRE (1U << 7)
#define TIM5_CR2 (*(volatile uint8_t *)(TIM5_BASE + 0x01))
#define TIM5_CR2_MMS_OFFSET 4
#define TIM5_CR2_MMS_MASK 0x07
#define TIM5_SMCR (*(volatile uint8_t *)(TIM5_BASE + 0x02))
#define TIM5_SMCR_SMS_OFFSET 0
#define TIM5_SMCR_SMS_MASK 0x07
#define TIM5_SMCR_TS_OFFSET 4
#define TIM5_SMCR_TS_MASK 0x07
#define TIM5_SMCR_MSM (1U << 7)
#define TIM5_IER (*(volatile uint8_t *)(TIM5_BASE + 0x03))
#define TIM5_IER_UIE (1U << 0)
#define TIM5_IER_CC1IE (1U << 1)
#define TIM5_IER_CC2IE (1U << 2)
#define TIM5_IER_CC3IE (1U << 3)
#define TIM5_IER_TIE (1U << 6)
#define TIM5_SR1 (*(volatile uint8_t *)(TIM5_BASE + 0x04))
#define TIM5_SR1_UIF (1U << 0)
#define TIM5_SR1_CC1IF (1U << 1)
#define TIM5_SR1_CC2IF (1U << 2)
#define TIM5_SR1_CC3IF (1U << 3)
#define TIM5_SR1_TIF (1U << 6)
#define TIM5_SR2 (*(volatile uint8_t *)(TIM5_BASE + 0x05))
#define TIM5_SR2_CC1OF (1U << 1)
#define TIM5_SR2_CC2OF (1U << 2)
#define TIM5_SR2_CC3OF (1U << 3)
#define TIM5_EGR (*(volatile uint8_t *)(TIM5_BASE + 0x06))
#define TIM5_EGR_CC1G (1U << 1)
#define TIM5_EGR_CC2G (1U << 2)
#define TIM5_EGR_CC3G (1U << 3)
#define TIM5_EGR_TG (1U << 6)
#define TIM5_CCMR1 (*(volatile uint8_t *)(TIM5_BASE + 0x07))
#define TIM5_CCMR1_CC1S_OFFSET 0
#define TIM5_CCMR1_CC1S_MASK 0x3
#define TIM5_CCMR1_CC1PE (1U << 3)
#define TIM5_CCMR1_OC1M_OFFSET 4
#define TIM5_CCMR1_OC1M_MASK 0x7
#define TIM5_CCMR1_CC1S_OFFSET 0
#define TIM5_CCMR1_CC1S_MASK 0x3
#define TIM5_CCMR1_IC1PSC_OFFSET 2
#define TIM5_CCMR1_IC1PSC_MASK 0x3
#define TIM5_CCMR1_IC1F_OFFSET 4
#define TIM5_CCMR1_IC1F_MASK 0xf
#define TIM5_CCMR2 (*(volatile uint8_t *)(TIM5_BASE + 0x08))
#define TIM5_CCMR2_CC2S_OFFSET 0
#define TIM5_CCMR2_CC2S_MASK 0x3
#define TIM5_CCMR2_CC2PE (1U << 3)
#define TIM5_CCMR2_OC2M_OFFSET 4
#define TIM5_CCMR2_OC2M_MASK 0x7
#define TIM5_CCMR2_CC2S_OFFSET 0
#define TIM5_CCMR2_CC2S_MASK 0x3
#define TIM5_CCMR2_IC2PSC_OFFSET 2
#define TIM5_CCMR2_IC2PSC_MASK 0x3
#define TIM5_CCMR2_IC2F_OFFSET 4
#define TIM5_CCMR2_IC2F_MASK 0xf
#define TIM5_CCMR3 (*(volatile uint8_t *)(TIM5_BASE + 0x09))
#define TIM5_CCMR3_CC3S_OFFSET 0
#define TIM5_CCMR3_CC3S_MASK 0x3
#define TIM5_CCMR3_CC3PE (1U << 3)
#define TIM5_CCMR3_OC3M_OFFSET 4
#define TIM5_CCMR3_OC3M_MASK 0x7
#define TIM5_CCMR3_CC3S_OFFSET 0
#define TIM5_CCMR3_CC3S_MASK 0x3
#define TIM5_CCMR3_IC3PSC_OFFSET 2
#define TIM5_CCMR3_IC3PSC_MASK 0x3
#define TIM5_CCMR3_IC3F_OFFSET 4
#define TIM5_CCMR3_IC3F_MASK 0xf
#define TIM5_CCER1 (*(volatile uint8_t *)(TIM5_BASE + 0x0A))
#define TIM5_CCER1_CC1E (1U << 0)
#define TIM5_CCER1_CC1P (1U << 1)
#define TIM5_CCER1_CC2E (1U << 4)
#define TIM5_CCER1_CC2P (1U << 5)
#define TIM5_CCER2 (*(volatile uint8_t *)(TIM5_BASE + 0x0B))
#define TIM5_CCER2_CC3E (1U << 0)
#define TIM5_CCER2_CC3P (1U << 1)
#define TIM5_CNTRH (*(volatile uint8_t *)(TIM5_BASE + 0x0C))
#define TIM5_CNTRL (*(volatile uint8_t *)(TIM5_BASE + 0x0D))
#define TIM5_PSCR (*(volatile uint8_t *)(TIM5_BASE + 0x0E))
#define TIM5_PSCR_OFFSET 0
#define TIM5_PSCR_MASK 0xf
#define TIM5_ARRH (*(volatile uint8_t *)(TIM5_BASE + 0x0F))
#define TIM5_ARRL (*(volatile uint8_t *)(TIM5_BASE + 0x10))
#define TIM5_CCR1H (*(volatile uint8_t *)(TIM5_BASE + 0x11))
#define TIM5_CCR1L (*(volatile uint8_t *)(TIM5_BASE + 0x12))
#define TIM5_CCR2H (*(volatile uint8_t *)(TIM5_BASE + 0x13))
#define TIM5_CCR2L (*(volatile uint8_t *)(TIM5_BASE + 0x14))
#define TIM5_CCR3H (*(volatile uint8_t *)(TIM5_BASE + 0x15))
#define TIM5_CCR3L (*(volatile uint8_t *)(TIM5_BASE + 0x16))

// Block: TIM6
#define TIM6_CR1 (*(volatile uint8_t *)(TIM6_BASE + 0x00))
#define TIM6_CR2 (*(volatile uint8_t *)(TIM6_BASE + 0x01))
#define TIM6_SMCR (*(volatile uint8_t *)(TIM6_BASE + 0x02))
#define TIM6_IER (*(volatile uint8_t *)(TIM6_BASE + 0x03))
#define TIM6_SR1 (*(volatile uint8_t *)(TIM6_BASE + 0x04))
#define TIM6_EGR (*(volatile uint8_t *)(TIM6_BASE + 0x05))
#define TIM6_CNTR (*(volatile uint8_t *)(TIM6_BASE + 0x06))
#define TIM6_PSCR (*(volatile uint8_t *)(TIM6_BASE + 0x07))
#define TIM6_ARR (*(volatile uint8_t *)(TIM6_BASE + 0x08))

// Block: ADC
typedef union {
	struct {
		uint8_t DBH:8; /*!< bit 0..7: Data bits high */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} ADC_DBxRH_type; /*!< ADC data buffer register x high (ADC_DBxRH) (x=0..7 or 0..9 ) */

typedef union {
	struct {
		uint8_t DBL:8; /*!< bit 0..7: Data bits low */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} ADC_DBxRL_type; /*!< ADC data buffer register x low (ADC_DBxRL) (x=0..7 or 0..9 ) */

typedef union {
	struct {
		uint8_t CH:4; /*!< bit 0..3: Channel selection bits */
		uint8_t AWDIE:1; /*!< bit 4: Analog watchdog interrupt enable */
		uint8_t EOCIE:1; /*!< bit 5: Interrupt enable for EOC */
		uint8_t AWD:1; /*!< bit 6: Analog Watchdog flag */
		uint8_t EOC:1; /*!< bit 7: End of conversion */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} ADC_CSR_type; /*!< ADC control/status register (ADC_CSR) */

typedef union {
	struct {
		uint8_t ADON:1; /*!< bit 0: A/D Converter on/off */
		uint8_t CONT:1; /*!< bit 1: Continuous conversion */
		uint8_t :2; /*!< bit 2..3: Reserved */
		uint8_t SPSEL:3; /*!< bit 4..6: Prescaler selection */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} ADC_CR1_type; /*!< ADC configuration register 1 (ADC_CR1) */

typedef union {
	struct {
		uint8_t :1; /*!< bit 0: Reserved */
		uint8_t SCAN:1; /*!< bit 1: Scan mode enable */
		uint8_t :1; /*!< bit 2: Reserved */
		uint8_t ALIGN:1; /*!< bit 3: Data alignment */
		uint8_t EXTSEL:2; /*!< bit 4..5: External event selection */
		uint8_t EXTTRIG:1; /*!< bit 6: External trigger enable */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} ADC_CR2_type; /*!< ADC configuration register 2 (ADC_CR2) */

typedef union {
	struct {
		uint8_t :6; /*!< bit 0..5: Reserved */
		uint8_t OVR:1; /*!< bit 6: Overrun flag */
		uint8_t DBUF:1; /*!< bit 7: Data buffer enable */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} ADC_CR3_type; /*!< ADC configuration register 3 (ADC_CR3) */

typedef union {
	struct {
		uint8_t DH:8; /*!< bit 0..7: Data bits high */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} ADC_DRH_type; /*!< ADC data register high (ADC_DRH) */

typedef union {
	struct {
		uint8_t DL:8; /*!< bit 0..7: Data bits low */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} ADC_DRL_type; /*!< ADC data register low (ADC_DRL) */

typedef union {
	struct {
		uint8_t TD15_8:8; /*!< bit 0..7: Schmitt trigger disable high */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} ADC_TDRH_type; /*!< ADC Schmitt trigger disable register high (ADC_TDRH) */

typedef union {
	struct {
		uint8_t TD7_0:8; /*!< bit 0..7: Schmitt trigger disable low */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} ADC_TDRL_type; /*!< ADC Schmitt trigger disable register low (ADC_TDRL) */

typedef union {
	struct {
		uint8_t HT9_2:8; /*!< bit 0..7: Analog Watchdog High Voltage threshold MSB */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} ADC_HTRH_type; /*!< ADC high threshold register high (ADC_HTRH) */

typedef union {
	struct {
		uint8_t HT1_0:2; /*!< bit 0..1: Analog Watchdog High Voltage threshold LSB */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} ADC_HTRL_type; /*!< ADC high threshold register low (ADC_HTRL) */

typedef union {
	struct {
		uint8_t LT9_2:8; /*!< bit 0..7: Analog watchdog low voltage threshold MSB */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} ADC_LTRH_type; /*!< ADC low threshold register high (ADC_LTRH) */

typedef union {
	struct {
		uint8_t LT1_0:2; /*!< bit 0..1: Analog watchdog low voltage threshold LSB */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} ADC_LTRL_type; /*!< ADC low threshold register low (ADC_LTRL) */

typedef union {
	struct {
		uint8_t AWS9_2:2; /*!< bit 0..1: Analog watchdog status flags 9:8 */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} ADC_AWSRH_type; /*!< ADC watchdog status register high (ADC_AWSRH) */

typedef union {
	struct {
		uint8_t AWS1_0:8; /*!< bit 0..7: Analog watchdog status flags 7:0 */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} ADC_AWSRL_type; /*!< ADC watchdog status register low (ADC_AWSRL) */

typedef union {
	struct {
		uint8_t AWEN9_8:2; /*!< bit 0..1: Analog watchdog enable bits 9:8 */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} ADC_AWCRH_type; /*!< ADC watchdog control register high (ADC_AWCRH) */

typedef union {
	struct {
		uint8_t AWEN7_0:8; /*!< bit 0..7: Analog watchdog enable bits 7:0 */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} ADC_AWCRL_type; /*!< ADC watchdog status register low (ADC_AWCRL) */

// Block: ADC1
typedef struct {
	volatile ADC_DBxRH_type DB0RH;
	volatile ADC_DBxRL_type DB0RL;
	const uint8_t reseved0[12];
	volatile ADC_DBxRH_type DB7RH;
	volatile ADC_DBxRL_type DB7RL;
	volatile ADC_DBxRH_type DB8RH;
	volatile ADC_DBxRL_type DB8RL;
	volatile ADC_DBxRH_type DB9RH;
	volatile ADC_DBxRL_type DB9RL;
	const uint8_t reseved1[12];
	volatile ADC_CSR_type CSR;
	volatile ADC_CR1_type CR1;
	volatile ADC_CR2_type CR2;
	volatile ADC_CR3_type CR3;
	volatile ADC_DRH_type DRH;
	volatile ADC_DRL_type DRL;
	volatile ADC_TDRH_type TDRH;
	volatile ADC_TDRL_type TDRL;
	volatile ADC_HTRH_type HTRH;
	volatile ADC_HTRL_type HTRL;
	volatile ADC_LTRH_type LTRH;
	volatile ADC_LTRL_type LTRL;
	volatile ADC_AWSRH_type AWSRH;
	volatile ADC_AWSRL_type AWSRL;
	volatile ADC_AWCRH_type AWCRH;
	volatile ADC_AWCRL_type AWCRL;
} ADC1_type;
#define ADC1_BASE 0x53E0
#define ADC1 ((ADC1_type*)ADC1_BASE)
#define ADC1_DB0RH (*(volatile uint8_t *)(ADC1_BASE + 0x00))
#define ADC1_DB0RL (*(volatile uint8_t *)(ADC1_BASE + 0x01))
#define ADC1_DB7RH (*(volatile uint8_t *)(ADC1_BASE + 0x0E))
#define ADC1_DB7RL (*(volatile uint8_t *)(ADC1_BASE + 0x0F))
#define ADC1_DB8RH (*(volatile uint8_t *)(ADC1_BASE + 0x10))
#define ADC1_DB8RL (*(volatile uint8_t *)(ADC1_BASE + 0x11))
#define ADC1_DB9RH (*(volatile uint8_t *)(ADC1_BASE + 0x12))
#define ADC1_DB9RL (*(volatile uint8_t *)(ADC1_BASE + 0x12))
#define ADC1_CSR (*(volatile uint8_t *)(ADC1_BASE + 0x20))
#define ADC1_CR1 (*(volatile uint8_t *)(ADC1_BASE + 0x21))
#define ADC1_CR2 (*(volatile uint8_t *)(ADC1_BASE + 0x22))
#define ADC1_CR3 (*(volatile uint8_t *)(ADC1_BASE + 0x23))
#define ADC1_DRH (*(volatile uint8_t *)(ADC1_BASE + 0x24))
#define ADC1_DRL (*(volatile uint8_t *)(ADC1_BASE + 0x25))
#define ADC1_TDRH (*(volatile uint8_t *)(ADC1_BASE + 0x26))
#define ADC1_TDRL (*(volatile uint8_t *)(ADC1_BASE + 0x27))
#define ADC1_HTRH (*(volatile uint8_t *)(ADC1_BASE + 0x28))
#define ADC1_HTRL (*(volatile uint8_t *)(ADC1_BASE + 0x29))
#define ADC1_LTRH (*(volatile uint8_t *)(ADC1_BASE + 0x2A))
#define ADC1_LTRL (*(volatile uint8_t *)(ADC1_BASE + 0x2B))
#define ADC1_AWSRH (*(volatile uint8_t *)(ADC1_BASE + 0x2C))
#define ADC1_AWSRL (*(volatile uint8_t *)(ADC1_BASE + 0x2D))
#define ADC1_AWCRH (*(volatile uint8_t *)(ADC1_BASE + 0x2E))
#define ADC1_AWCRL (*(volatile uint8_t *)(ADC1_BASE + 0x2F))

// Block: ADC2
typedef struct {
	const uint8_t reseved0[0x20];
	volatile ADC_CSR_type CSR;
	volatile ADC_CR1_type CR1;
	volatile ADC_CR2_type CR2;
	volatile ADC_CR3_type CR3;
	volatile ADC_DRH_type DRH;
	volatile ADC_DRL_type DRL;
	volatile ADC_TDRH_type TDRH;
	volatile ADC_TDRL_type TDRL;
} ADC2_type;
#define ADC2_BASE 0xFFFF
#define ADC2 ((ADC2_type*)ADC2_BASE)
#define ADC2_CSR (*(volatile uint8_t *)(ADC1_BASE + 0x20))
#define ADC2_CR1 (*(volatile uint8_t *)(ADC1_BASE + 0x21))
#define ADC2_CR2 (*(volatile uint8_t *)(ADC1_BASE + 0x22))
#define ADC2_CR3 (*(volatile uint8_t *)(ADC1_BASE + 0x23))
#define ADC2_DRH (*(volatile uint8_t *)(ADC1_BASE + 0x24))
#define ADC2_DRL (*(volatile uint8_t *)(ADC1_BASE + 0x25))
#define ADC2_TDRH (*(volatile uint8_t *)(ADC1_BASE + 0x26))
#define ADC2_TDRL (*(volatile uint8_t *)(ADC1_BASE + 0x27))

// CPU/SWIM/debug module/interrupt controller registers
// Block: CPU
typedef union {
	struct {
		const uint8_t C:1; /*!< bit 0: Carry */
		const uint8_t Z:1; /*!< bit 1: Zero */
		const uint8_t N:1; /*!< bit 2: Negative */
		uint8_t I0:1; /*!< bit 3: Interrupt mask level 0 */
		const uint8_t H:1; /*!< bit 4: Half carry bit */
		uint8_t I1:1; /*!< bit 5: Interrupt mask level 1 */
		uint8_t :1; /*!< bit 6: Reserved */
		const uint8_t V:1; /*!< bit 7: Overflow */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} CPU_CC_type; /*!< CPU registers */

typedef struct {
	volatile uint8_t A;
	volatile uint8_t PCE;
	volatile uint8_t PCH;
	volatile uint8_t PCL;
	volatile uint8_t XH;
	volatile uint8_t XL;
	volatile uint8_t XY;
	volatile uint8_t YH;
	volatile uint8_t YL;
	volatile uint8_t SPH;
	volatile uint8_t SPL;
	volatile CPU_CC_type CC;
} CPU_type;

#define CPU_BASE 0x7F00
#define CPU ((CPU_type*)CPU_BASE)
#define CPU_A (*(volatile uint8_t *)(CPU_BASE + 0x00))
#define CPU_PCE (*(volatile uint8_t *)(CPU_BASE + 0x01))
#define CPU_PCH (*(volatile uint8_t *)(CPU_BASE + 0x02))
#define CPU_PCL (*(volatile uint8_t *)(CPU_BASE + 0x03))
#define CPU_XH (*(volatile uint8_t *)(CPU_BASE + 0x04))
#define CPU_XL (*(volatile uint8_t *)(CPU_BASE + 0x05))
#define CPU_YH (*(volatile uint8_t *)(CPU_BASE + 0x06))
#define CPU_YL (*(volatile uint8_t *)(CPU_BASE + 0x07))
#define CPU_SPH (*(volatile uint8_t *)(CPU_BASE + 0x08))
#define CPU_SPL (*(volatile uint8_t *)(CPU_BASE + 0x09))
#define CPU_CCR (*(volatile uint8_t *)(CPU_BASE + 0x0A))
#define CPU_CCR_C (1U << 0)
#define CPU_CCR_Z (1U << 1)
#define CPU_CCR_N (1U << 2)
#define CPU_CCR_I0 (1U << 3)
#define CPU_CCR_H (1U << 4)
#define CPU_CCR_I1 (1U << 5)
#define CPU_CCR_V (1U << 7)

typedef union {
	struct {
		uint8_t SWD:1; /*!< bit 0: SWIM disable */
		uint8_t AL:1; /*!< bit 1: Activation level */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} CFG_GCR_type; /*!< Global configuration register (CFG_GCR) */

#define CFG_BASE 0x7F60
#define CFG_GCR ((volatile CFG_GCR_type*)CFG_BASE)

// Block: ITC
typedef union {
	struct {
		uint8_t VECT0SPR:2; /*!< bit 0..1: Vector x software priority bits */
		uint8_t VECT1SPR:2; /*!< bit 2..3: Vector x software priority bits */
		uint8_t VECT2SPR:2; /*!< bit 4..5: Vector x software priority bits */
		uint8_t VECT3SPR:2; /*!< bit 6..7: Vector x software priority bits */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} ITC_SPR1_type; /*!< Software priority register x (ITC_SPRx) */

typedef union {
	struct {
		uint8_t VECT4SPR:2; /*!< bit 0..1: Vector x software priority bits */
		uint8_t VECT5SPR:2; /*!< bit 2..3: Vector x software priority bits */
		uint8_t VECT6SPR:2; /*!< bit 4..5: Vector x software priority bits */
		uint8_t VECT7SPR:2; /*!< bit 6..7: Vector x software priority bits */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} ITC_SPR2_type; /*!< Software priority register x (ITC_SPRx) */

typedef union {
	struct {
		uint8_t VECT8SPR:2; /*!< bit 0..1: Vector x software priority bits */
		uint8_t VECT9SPR:2; /*!< bit 2..3: Vector x software priority bits */
		uint8_t VECT10SPR:2; /*!< bit 4..5: Vector x software priority bits */
		uint8_t VECT11SPR:2; /*!< bit 6..7: Vector x software priority bits */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} ITC_SPR3_type; /*!< Software priority register x (ITC_SPRx) */

typedef union {
	struct {
		uint8_t VECT12SPR:2; /*!< bit 0..1: Vector x software priority bits */
		uint8_t VECT13SPR:2; /*!< bit 2..3: Vector x software priority bits */
		uint8_t VECT14SPR:2; /*!< bit 4..5: Vector x software priority bits */
		uint8_t VECT15SPR:2; /*!< bit 6..7: Vector x software priority bits */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} ITC_SPR4_type; /*!< Software priority register x (ITC_SPRx) */

typedef union {
	struct {
		uint8_t VECT16SPR:2; /*!< bit 0..1: Vector x software priority bits */
		uint8_t VECT17SPR:2; /*!< bit 2..3: Vector x software priority bits */
		uint8_t VECT18SPR:2; /*!< bit 4..5: Vector x software priority bits */
		uint8_t VECT19SPR:2; /*!< bit 6..7: Vector x software priority bits */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} ITC_SPR5_type; /*!< Software priority register x (ITC_SPRx) */

typedef union {
	struct {
		uint8_t VECT20SPR:2; /*!< bit 0..1: Vector x software priority bits */
		uint8_t VECT21SPR:2; /*!< bit 2..3: Vector x software priority bits */
		uint8_t VECT22SPR:2; /*!< bit 4..5: Vector x software priority bits */
		uint8_t VECT23SPR:2; /*!< bit 6..7: Vector x software priority bits */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} ITC_SPR6_type; /*!< Software priority register x (ITC_SPRx) */

typedef union {
	struct {
		uint8_t VECT24SPR:2; /*!< bit 0..1: Vector x software priority bits */
		uint8_t VECT25SPR:2; /*!< bit 2..3: Vector x software priority bits */
		uint8_t VECT26SPR:2; /*!< bit 4..5: Vector x software priority bits */
		uint8_t VECT27SPR:2; /*!< bit 6..7: Vector x software priority bits */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} ITC_SPR7_type; /*!< Software priority register x (ITC_SPRx) */

typedef union {
	struct {
		uint8_t VECT28SPR:2; /*!< bit 0..1: Vector x software priority bits */
		uint8_t VECT29SPR:2; /*!< bit 2..3: Vector x software priority bits */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} ITC_SPR8_type; /*!< Software priority register x (ITC_SPRx) */

typedef struct {
	volatile ITC_SPR1_type SPR1;
	volatile ITC_SPR2_type SPR2;
	volatile ITC_SPR3_type SPR3;
	volatile ITC_SPR4_type SPR4;
	volatile ITC_SPR5_type SPR5;
	volatile ITC_SPR6_type SPR6;
	volatile ITC_SPR7_type SPR7;
	volatile ITC_SPR8_type SPR8;
} ITC_type;

#define ITC_BASE 0x007F70
#define ITC ((ITC_type*)ITC_BASE)
#define ITC_SPR1 (*(volatile uint8_t *)(ITC_BASE + 0x00))
#define ITC_SPR1_VECT0SPR_OFFSET 0
#define ITC_SPR1_VECT0SPR_MASK 3
#define ITC_SPR1_VECT1SPR_OFFSET 2
#define ITC_SPR1_VECT1SPR_MASK 3
#define ITC_SPR1_VECT2SPR_OFFSET 4
#define ITC_SPR1_VECT2SPR_MASK 3
#define ITC_SPR1_VECT3SPR_OFFSET 6
#define ITC_SPR1_VECT3SPR_MASK 3
#define ITC_SPR2 (*(volatile uint8_t *)(ITC_BASE + 0x01))
#define ITC_SPR2_VECT4SPR_OFFSET 0
#define ITC_SPR2_VECT4SPR_MASK 3
#define ITC_SPR2_VECT5SPR_OFFSET 2
#define ITC_SPR2_VECT5SPR_MASK 3
#define ITC_SPR2_VECT6SPR_OFFSET 4
#define ITC_SPR2_VECT6SPR_MASK 3
#define ITC_SPR2_VECT7SPR_OFFSET 6
#define ITC_SPR2_VECT7SPR_MASK 3
#define ITC_SPR3 (*(volatile uint8_t *)(ITC_BASE + 0x02))
#define ITC_SPR3_VECT8SPR_OFFSET 0
#define ITC_SPR3_VECT8SPR_MASK 3
#define ITC_SPR3_VECT9SPR_OFFSET 2
#define ITC_SPR3_VECT9SPR_MASK 3
#define ITC_SPR3_VECT10SPR_OFFSET 4
#define ITC_SPR3_VECT10SPR_MASK 3
#define ITC_SPR3_VECT11SPR_OFFSET 6
#define ITC_SPR3_VECT11SPR_MASK 3
#define ITC_SPR4 (*(volatile uint8_t *)(ITC_BASE + 0x03))
#define ITC_SPR4_VECT12SPR_OFFSET 0
#define ITC_SPR4_VECT12SPR_MASK 3
#define ITC_SPR4_VECT13SPR_OFFSET 2
#define ITC_SPR4_VECT13SPR_MASK 3
#define ITC_SPR4_VECT14SPR_OFFSET 4
#define ITC_SPR4_VECT14SPR_MASK 3
#define ITC_SPR4_VECT15SPR_OFFSET 6
#define ITC_SPR4_VECT15SPR_MASK 3
#define ITC_SPR5 (*(volatile uint8_t *)(ITC_BASE + 0x04))
#define ITC_SPR5_VECT16SPR_OFFSET 0
#define ITC_SPR5_VECT16SPR_MASK 3
#define ITC_SPR5_VECT17SPR_OFFSET 2
#define ITC_SPR5_VECT17SPR_MASK 3
#define ITC_SPR5_VECT18SPR_OFFSET 4
#define ITC_SPR5_VECT18SPR_MASK 3
#define ITC_SPR5_VECT19SPR_OFFSET 6
#define ITC_SPR5_VECT19SPR_MASK 3
#define ITC_SPR6 (*(volatile uint8_t *)(ITC_BASE + 0x05))
#define ITC_SPR6_VECT20SPR_OFFSET 0
#define ITC_SPR6_VECT20SPR_MASK 3
#define ITC_SPR6_VECT21SPR_OFFSET 2
#define ITC_SPR6_VECT21SPR_MASK 3
#define ITC_SPR6_VECT22SPR_OFFSET 4
#define ITC_SPR6_VECT22SPR_MASK 3
#define ITC_SPR6_VECT23SPR_OFFSET 6
#define ITC_SPR6_VECT23SPR_MASK 3
#define ITC_SPR7 (*(volatile uint8_t *)(ITC_BASE + 0x06))
#define ITC_SPR7_VECT24SPR_OFFSET 0
#define ITC_SPR7_VECT24SPR_MASK 3
#define ITC_SPR7_VECT25SPR_OFFSET 2
#define ITC_SPR7_VECT25SPR_MASK 3
#define ITC_SPR7_VECT26SPR_OFFSET 4
#define ITC_SPR7_VECT26SPR_MASK 3
#define ITC_SPR7_VECT27SPR_OFFSET 6
#define ITC_SPR7_VECT27SPR_MASK 3
#define ITC_SPR8 (*(volatile uint8_t *)(ITC_BASE + 0x07))
#define ITC_SPR8_VECT28SPR_OFFSET 0
#define ITC_SPR8_VECT28SPR_MASK 3
#define ITC_SPR8_VECT29SPR_OFFSET 2
#define ITC_SPR8_VECT29SPR_MASK 3

// Block: SWIM
typedef union {
	struct {
		uint8_t PRI:1; /*!< bit 0: SWIM access priority */
		const uint8_t HSIT:1; /*!< bit 1: High speed internal clock is trimmed */
#pragma push_macro("RST")
#undef RST
		uint8_t RST:1; /*!< bit 2: SWIM reset control bit */
#pragma pop_macro("RST")
		uint8_t OSCOFF:1; /*!< bit 3: Oscillators off control bit */
		uint8_t HS:1; /*!< bit 4: High speed */
		uint8_t SWIM_DM:1; /*!< bit 5: SWIM for debug module */
		const uint8_t NO_ACCESS:1; /*!< bit 6: Bus not accessible */
		uint8_t SAFE_MASK:1; /*!< bit 7: Mask internal RESET sources */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} SWIM_CSR_type; /*!<  */
#define SWIM_BASE 0x7F80
#define SWIM_CSR ((volatile SWIM_CSR_type*)SWIM_BASE)

// Block: DM
typedef union {
	struct {
		uint8_t BK1:8; /*!< bit 0..7: Breakpoint 1 extended byte value */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} DM_BKR1E_type; /*!< DM breakpoint register 1 extended byte (DM_BKR1E) */

typedef union {
	struct {
		uint8_t BK1:8; /*!< bit 0..7: Breakpoint 1 high byte value */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} DM_BKR1H_type; /*!< DM breakpoint register 1 high byte (DM_BKR1H) */

typedef union {
	struct {
		uint8_t BK1:8; /*!< bit 0..7: Breakpoint 1 low byte value */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} DM_BKR1L_type; /*!< DM breakpoint register 1 low byte (DM_BKR1L) */

typedef union {
	struct {
		uint8_t BK2:8; /*!< bit 0..7: Breakpoint 2 extended byte value */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} DM_BKR2E_type; /*!< DM breakpoint register 2 extended byte (DM_BKR2E) */

typedef union {
	struct {
		uint8_t BK2:8; /*!< bit 0..7: Breakpoint 2 high byte value */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} DM_BKR2H_type; /*!< DM breakpoint register 2 high byte (DM_BKR2H) */

typedef union {
	struct {
		uint8_t BK2:8; /*!< bit 0..7: Breakpoint 2 low byte value */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} DM_BKR2L_type; /*!< DM breakpoint register 2 low byte (DM_BKR2L) */

typedef union {
	struct {
		uint8_t :1; /*!< bit 0: Reserved */
		uint8_t BIW:1; /*!< bit 1: Break on write control */
		uint8_t BIR:1; /*!< bit 2: Break on read control */
		uint8_t BC:3; /*!< bit 3..5: Breakpoint control */
		uint8_t :1; /*!< bit 6: Reserved */
		uint8_t WDGOFF:1; /*!< bit 7: Watchdog control enable */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} DM_CR1_type; /*!< DM control register 1 (DM_CR1) */

typedef union {
	struct {
		uint8_t FV_RAM:1; /*!< bit 0: Remap vector table in RAM */
		uint8_t :1; /*!< bit 1: Reserved */
		uint8_t FV_ROM:1; /*!< bit 2: Remap vector table in ROM */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} DM_CR2_type; /*!< DM control register 2 (DM_CR2) */

typedef union {
	struct {
		uint8_t :1; /*!< bit 0: Reserved */
		const uint8_t BK1F:1; /*!< bit 1: Breakpoint 1 flag */
		const uint8_t BK2F:1; /*!< bit 2: Breakpoint 2 flag */
		const uint8_t BRW:1; /*!< bit 3: Break on read/write flag */
#pragma push_macro("RST")
#undef RST
		const uint8_t RST:1; /*!< bit 4: Reset flag */
#pragma pop_macro("RST")
		const uint8_t STF:1; /*!< bit 5: Step flag */
		uint8_t STE:1; /*!< bit 6: Step mode enable */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} DM_CSR1_type; /*!< DM control/status register 1 (DM_CSR1) */

typedef union {
	struct {
		uint8_t FLUSH:1; /*!< bit 0: Flush decode */
		uint8_t :2; /*!< bit 1..2: Reserved */
		uint8_t STALL:1; /*!< bit 3: CPU stall control bit */
		const uint8_t SWBKF:1; /*!< bit 4: Software breakpoint status bit */
		uint8_t SWBKE:1; /*!< bit 5: Software breakpoint control bit */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} DM_CSR2_type; /*!< DM control/status register 2 (DM_CSR2) */

typedef union {
	struct {
		uint8_t ENFCT0:1; /*!< bit 0: Enable function */
		uint8_t ENFCT1:1; /*!< bit 0: Enable function */
		uint8_t ENFCT2:1; /*!< bit 0: Enable function */
		uint8_t ENFCT3:1; /*!< bit 0: Enable function */
		uint8_t ENFCT4:1; /*!< bit 0: Enable function */
		uint8_t ENFCT5:1; /*!< bit 0: Enable function */
		uint8_t ENFCT6:1; /*!< bit 0: Enable function */
		uint8_t ENFCT7:1; /*!< bit 0: Enable function */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} DM_ENFCTR_type; /*!< DM enable function register (DM_ENFCTR) */

typedef struct {
	volatile DM_BKR1E_type BK1RE;
	volatile DM_BKR1H_type BK1RH;
	volatile DM_BKR1L_type BK1RL;
	volatile DM_BKR2E_type BK2RE;
	volatile DM_BKR2H_type BK2RH;
	volatile DM_BKR2L_type BK2RL;
	volatile DM_CR1_type CR1;
	volatile DM_CR2_type CR2;
	volatile DM_CSR1_type CSR1;
	volatile DM_CSR2_type CSR2;
	volatile DM_ENFCTR_type ENFCTR;
} DM_type;

#define DM_BASE 0x7F90
#define DM ((DM_type*)DM_BASE)
#define DM_BK1RE (*(volatile uint8_t *)(DM_BASE + 0x00))
#define DM_BK1RH (*(volatile uint8_t *)(DM_BASE + 0x01))
#define DM_BK1RL (*(volatile uint8_t *)(DM_BASE + 0x02))
#define DM_BK2RE (*(volatile uint8_t *)(DM_BASE + 0x03))
#define DM_BK2RH (*(volatile uint8_t *)(DM_BASE + 0x04))
#define DM_BK2RL (*(volatile uint8_t *)(DM_BASE + 0x05))
#define DM_CR1 (*(volatile uint8_t *)(DM_BASE + 0x06))
#define DM_CR2 (*(volatile uint8_t *)(DM_BASE + 0x07))
#define DM_CSR1 (*(volatile uint8_t *)(DM_BASE + 0x08))
#define DM_CSR2 (*(volatile uint8_t *)(DM_BASE + 0x09))
#define DM_ENFCTR (*(volatile uint8_t *)(DM_BASE + 0x0A))

// Interrupt vector mapping
#define IRQ_TLI 0 // External top level interrupt
#define IRQ_AWU 1 // Auto wake up from halt
#define IRQ_CLK 2 // Clock controller
#define IRQ_EXTI0 3 // Port A external interrupts
#define IRQ_EXTI1 4 // Port B external interrupts
#define IRQ_EXTI2 5 // Port C external interrupts
#define IRQ_EXTI3 6 // Port D external interrupts
#define IRQ_EXTI4 7 // Port E external interrupts
#define IRQ_SPI 10 // End of transfer
#define IRQ_TIM1_UO 11 // TIM1 update/overflow/underflow/trigger/break
#define IRQ_TIM1_CC 12 // TIM1 capture/compare
#define IRQ_TIM2_UO 13 // TIM2 update/overflow
#define IRQ_TIM2_CC 14 // TIM2 capture/compare
#define IRQ_UART1_TX 17 // Tx complete
#define IRQ_UART1_RX 18 // Receive register DATA FULL
#define IRQ_I2C 19 // I2C interrupt
#define IRQ_ADC1 22 // ADC1 end of conversion/analog watchdog interrupt
#define IRQ_TIM4 23 // TIM4 update/overflow
#define IRQ_FLASH 24 // EOP/WR_PG_DIS

// Option byte
typedef union {
	struct {
		uint8_t ROP:8; /*!< bit 0..7: Memory readout protection */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} OPT_OPT0_type; /*!<  */

typedef union {
	struct {
		uint8_t UBS:8; /*!< bit 0..7: User boot code area */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} OPT_OPT1_type; /*!<  */

typedef union {
	struct {
		uint8_t NUBS:8; /*!< bit 0..7: User boot code area */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} OPT_NOPT1_type; /*!<  */

// NOTE: OPT2 fields depends on device's pin count
typedef union {
	struct {
		uint8_t AFR0:1; /*!< bit 0: Alternate function remapping option 0 */
		uint8_t AFR1:1; /*!< bit 1: Alternate function remapping option 1 */
		uint8_t AFR2:1; /*!< bit 2: Alternate function remapping option 2 */
		uint8_t AFR3:1; /*!< bit 3: Alternate function remapping option 3 */
		uint8_t AFR4:1; /*!< bit 4: Alternate function remapping option 4 */
		uint8_t AFR5:1; /*!< bit 5: Alternate function remapping option 5 */
		uint8_t AFR6:1; /*!< bit 6: Alternate function remapping option 6 */
		uint8_t AFR7:1; /*!< bit 7: Alternate function remapping option 7 */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} OPT_OPT2_type; /*!<  */

typedef union {
	struct {
		uint8_t NAFR0:1; /*!< bit 0: Alternate function remapping option 0 */
		uint8_t NAFR1:1; /*!< bit 1: Alternate function remapping option 1 */
		uint8_t NAFR2:1; /*!< bit 2: Alternate function remapping option 2 */
		uint8_t NAFR3:1; /*!< bit 3: Alternate function remapping option 3 */
		uint8_t NAFR4:1; /*!< bit 4: Alternate function remapping option 4 */
		uint8_t NAFR5:1; /*!< bit 5: Alternate function remapping option 5 */
		uint8_t NAFR6:1; /*!< bit 6: Alternate function remapping option 6 */
		uint8_t NAFR7:1; /*!< bit 7: Alternate function remapping option 7 */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} OPT_NOPT2_type; /*!<  */

typedef union {
	struct {
		uint8_t WWDG_HALT:1; /*!< bit 0: Window watchdog reset on halt */
		uint8_t WWDG_HW:1; /*!< bit 1: Window watchdog activation */
		uint8_t IWDG_HW:1; /*!< bit 2: Independent watchdog */
		uint8_t LSI_EN:1; /*!< bit 3: Low speed internal clock enable */
		uint8_t HSITRIM:1; /*!< bit 4: High speed internal clock trimming register size */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} OPT_OPT3_type; /*!<  */

typedef union {
	struct {
		uint8_t NWWDG_HALT:1; /*!< bit 0: Window watchdog reset on halt */
		uint8_t NWWDG_HW:1; /*!< bit 1: Window watchdog activation */
		uint8_t NIWDG_HW:1; /*!< bit 2: Independent watchdog */
		uint8_t NLSI_EN:1; /*!< bit 3: Low speed internal clock enable */
		uint8_t NHSITRIM:1; /*!< bit 4: High speed internal clock trimming register size */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} OPT_NOPT3_type; /*!<  */

typedef union {
	struct {
		uint8_t PRSC:2; /*!< bit 0..1: AWU clock prescaler  */
		uint8_t CKAWUSEL:1; /*!< bit 2: Auto wake-up unit/clock */
		uint8_t EXTCLK:1; /*!< bit 3: External clock selection */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} OPT_OPT4_type; /*!<  */

typedef union {
	struct {
		uint8_t NPRSC:2; /*!< bit 0..1: AWU clock prescaler  */
		uint8_t NCKAWUSEL:1; /*!< bit 2: Auto wake-up unit/clock */
		uint8_t NEXTCLK:1; /*!< bit 3: External clock selection */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} OPT_NOPT4_type; /*!<  */

typedef union {
	struct {
		uint8_t HSECNT:8; /*!< bit 0..7: HSE crystal oscillator stabilization time */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} OPT_OPT5_type; /*!<  */

typedef union {
	struct {
		uint8_t NHSECNT:8; /*!< bit 0..7: HSE crystal oscillator stabilization time */
	} fields; /*!< structure used for bit access */
	uint8_t reg; /*!< type used for register access */
} OPT_NOPT5_type; /*!<  */

typedef struct {
	volatile OPT_OPT0_type OPT0;
	volatile OPT_OPT1_type OPT1;
	volatile OPT_NOPT1_type NOPT1;
	volatile OPT_OPT2_type OPT2;
	volatile OPT_NOPT2_type NOPT2;
	volatile OPT_OPT3_type OPT3;
	volatile OPT_NOPT3_type NOPT3;
	volatile OPT_OPT4_type OPT4;
	volatile OPT_NOPT4_type NOPT4;
	volatile OPT_OPT5_type OPT5;
	volatile OPT_NOPT5_type NOPT5;
} OPT_type;

#define OPT_BASE 0x4800
#define OPT ((OPT_type*)OPT_BASE)
#define OPT_OPT0 (*(volatile uint8_t *)(OPT_BASE + 0x00))
#define ROP OPT_OPT0
#define OPT_OPT1 (*(volatile uint8_t *)(OPT_BASE + 0x01))
#define UBC OPT_OPT1
#define OPT_NOPT1 (*(volatile uint8_t *)(OPT_BASE + 0x02))
#define NUBC OPT_NOPT1
#define OPT_OPT2 (*(volatile uint8_t *)(OPT_BASE + 0x03))
#define AFR OPT_OPT2
#define AFR_AFR0 (1U << 0)
#define AFR_AFR1 (1U << 1)
#define AFR_AFR2 (1U << 2)
#define AFR_AFR3 (1U << 3)
#define AFR_AFR4 (1U << 4)
#define AFR_AFR5 (1U << 5)
#define AFR_AFR6 (1U << 6)
#define AFR_AFR7 (1U << 7)
#define OPT_NOPT2 (*(volatile uint8_t *)(OPT_BASE + 0x04))
#define NAFR OPT_NOPT2
#define AFR_NAFR0 (1U << 0)
#define AFR_NAFR1 (1U << 1)
#define AFR_NAFR2 (1U << 2)
#define AFR_NAFR3 (1U << 3)
#define AFR_NAFR4 (1U << 4)
#define AFR_NAFR5 (1U << 5)
#define AFR_NAFR6 (1U << 6)
#define AFR_NAFR7 (1U << 7)
#define OPT_OPT3 (*(volatile uint8_t *)(OPT_BASE + 0x05))
#define OPT3_WWDG_HALT (1U << 0)
#define OPT3_WWDG_HW (1U << 1)
#define OPT3_IWDG_HW (1U << 2)
#define OPT3_LSI_EN (1U << 3)
#define OPT3_HSI_TRIM (1U << 4)
#define OPT_NOPT3 (*(volatile uint8_t *)(OPT_BASE + 0x06))
#define NOPT3_NWWDG_HALT (1U << 0)
#define NOPT3_NWWDG_HW (1U << 1)
#define NOPT3_NIWDG_HW (1U << 2)
#define NOPT3_NLSI_EN (1U << 3)
#define NOPT3_NHSI_TRIM (1U << 4)
#define OPT_OPT4 (*(volatile uint8_t *)(OPT_BASE + 0x07))
#define OPT4_PRS_C0 (1U << 0)
#define OPT4_PRS_C1 (1U << 1)
#define OPT4_CKAWU_SEL (1U << 2)
#define OPT4_EXT_CLK (1U << 3)
#define OPT_NOPT4 (*(volatile uint8_t *)(OPT_BASE + 0x08))
#define NOPT4_NPRS_C0 (1U << 0)
#define NOPT4_NPRS_C1 (1U << 1)
#define NOPT4_NCKAWU_SEL (1U << 2)
#define NOPT4_NEXT_CLK (1U << 3)
#define OPT_OPT5 (*(volatile uint8_t *)(OPT_BASE + 0x09))
#define HSECNT OPT_OPT5
#define OPT_NOPT5 (*(volatile uint8_t *)(OPT_BASE + 0x0A))
#define NHSECNT OPT_NOPT5

// Unique ID
typedef struct {
	volatile uint8_t U_ID[12]; /*< 96-bit unique device identifier */
} U_ID_type; /*< Unique ID */

#define U_ID_BASE 0x48CD // PRODUCT this is the address for STM8S103
#define U_ID ((U_ID_type*)U_ID_BASE)

/* some useful assembly instructions */
// wait for event (go to sleep)
#define wfe() { __asm__("wfe\n"); }
// wait for interrupt (go to sleep)
#define wfi() { __asm__("wfi\n"); }
// go into halt mode
#define halt() { __asm__("halt\n"); }
// disable interrupts
#define sim() { __asm__("sim"); }
// enable interrupts
#define rim() { __asm__("rim"); }
